package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.almajid.kialeadgen.beans.Quality;
import com.almajid.kialeadgen.beans.ServiceLevelAgreement;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentSLAp6 extends Fragment {

    private ServiceLevelAgreement sla;
    private BarChart qsChart;
    private View rootView;
    private LinearLayout content_frame_nav;

    private TextView txtQTodayPct;
    private TextView txtQPct;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_sla_p6, container, false);

        txtQTodayPct = (TextView) rootView.findViewById(R.id.txtQTodayPct);
        txtQPct = (TextView) rootView.findViewById(R.id.txtQPct);

        qsChart = (BarChart) rootView.findViewById(R.id.qsChart);

/*
        content_frame_nav = (LinearLayout) ((FragmentActivity) rootView.getContext()).findViewById(R.id.content_frame_nav);
        content_frame_nav.setVisibility(View.GONE);
*/

        return rootView;
    }

    public void setSLA(ServiceLevelAgreement sla) {
        this.sla = sla;
    }

    public void updateLabels() {

        Quality quality = sla.getQuality();

        txtQTodayPct.setText("N.A.");
        txtQPct.setText(String.valueOf(quality.getQaqcPct()) + "%");
        if (quality.getQaqcTarget() > quality.getQaqcPct()) {
            txtQPct.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.rate_negative));
        } else {
            txtQPct.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.rate_positive));
        }
    }

    public void refreshChart() {
        qsChart.resetTracking();
        qsChart.setData(null);
        qsChart.invalidate();
    }

    public void updateChart() {

        Legend l = qsChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        l.setYOffset(0f);
        l.setYEntrySpace(0f);
        l.setTextSize(7f);


        qsChart.setDescription("");
        qsChart.setPinchZoom(false);
        qsChart.setDrawBarShadow(false);
        qsChart.setDrawGridBackground(false);
        qsChart.getAxisLeft().setDrawGridLines(false);
        qsChart.animateY(2500);
        qsChart.getAxisRight().setDrawLabels(false);
        qsChart.getAxisLeft().setDrawLabels(false);

        XAxis xAxis = qsChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(8f);
        xAxis.setSpaceBetweenLabels(0);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));

        YAxis lyaxis = qsChart.getAxisLeft();
        lyaxis.setTextSize(8f);
        lyaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));

        ArrayList<String> xVals = new ArrayList<>();
        xVals.add("1");
        xVals.add("2");
        xVals.add("3");
        xVals.add("4");
        xVals.add("5");
        xVals.add("6");
        xVals.add("7");
        xVals.add("8");
        xVals.add("9");
        xVals.add("10");

        //TQ
        ArrayList<BarEntry> yValsTQ = new ArrayList<>();
        BarEntry barEntry = new BarEntry(3, 0);
        yValsTQ.add(barEntry);
        barEntry = new BarEntry(2, 1);
        yValsTQ.add(barEntry);
        barEntry = new BarEntry(1, 2);
        yValsTQ.add(barEntry);
        barEntry = new BarEntry(4, 3);
        yValsTQ.add(barEntry);
        barEntry = new BarEntry(5, 4);
        yValsTQ.add(barEntry);
        barEntry = new BarEntry((float)4.5, 5);
        yValsTQ.add(barEntry);
        barEntry = new BarEntry((float)3.2, 6);
        yValsTQ.add(barEntry);
        barEntry = new BarEntry(7, 7);
        yValsTQ.add(barEntry);
        barEntry = new BarEntry(4, 8);
        yValsTQ.add(barEntry);
        barEntry = new BarEntry((float)6.8, 9);
        yValsTQ.add(barEntry);

        BarDataSet setTQ = new BarDataSet(yValsTQ, "TOTAL QUALITY");
        setTQ.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        setTQ.setColor(Color.parseColor("#FFA500"));
        setTQ.setDrawValues(true);
        setTQ.setValueTextSize(8f);
        setTQ.setValueFormatter(new PercentFormatter());


        //QA
        ArrayList<BarEntry> yValsQA = new ArrayList<>();
        BarEntry barEntryQA = new BarEntry(2, 0);
        yValsQA.add(barEntryQA);
        barEntryQA = new BarEntry(4, 1);
        yValsQA.add(barEntryQA);
        barEntryQA = new BarEntry((float)1.5, 2);
        yValsQA.add(barEntryQA);
        barEntryQA = new BarEntry(5, 3);
        yValsQA.add(barEntryQA);
        barEntryQA = new BarEntry(3, 4);
        yValsQA.add(barEntryQA);
        barEntryQA = new BarEntry(5, 5);
        yValsQA.add(barEntryQA);
        barEntryQA = new BarEntry((float)3.2, 6);
        yValsQA.add(barEntryQA);
        barEntryQA = new BarEntry(8, 7);
        yValsQA.add(barEntryQA);
        barEntryQA = new BarEntry(5, 8);
        yValsQA.add(barEntryQA);
        barEntryQA = new BarEntry(7, 9);
        yValsQA.add(barEntryQA);

        BarDataSet setQA = new BarDataSet(yValsQA, "QUALITY ASSURANCE");
        setQA.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        setQA.setColor(Color.parseColor("#FFFF00"));
        setQA.setDrawValues(true);
        setQA.setValueTextSize(8f);
        setQA.setValueFormatter(new PercentFormatter());


        //QC
        ArrayList<BarEntry> yValsQC = new ArrayList<>();
        BarEntry barEntryQC = new BarEntry(3, 0);
        yValsQC.add(barEntryQC);
        barEntryQC = new BarEntry(2, 1);
        yValsQC.add(barEntryQC);
        barEntryQC = new BarEntry(1, 2);
        yValsQC.add(barEntryQC);
        barEntryQC = new BarEntry(4, 3);
        yValsQC.add(barEntryQC);
        barEntryQC = new BarEntry(5, 4);
        yValsQC.add(barEntryQC);
        barEntryQC = new BarEntry((float)4.5, 5);
        yValsQC.add(barEntryQC);
        barEntryQC = new BarEntry((float)3.2, 6);
        yValsQC.add(barEntryQC);
        barEntryQC = new BarEntry(7, 7);
        yValsQC.add(barEntryQC);
        barEntryQC = new BarEntry(4, 8);
        yValsQC.add(barEntryQC);
        barEntryQC = new BarEntry((float)6.8, 9);
        yValsQC.add(barEntryQC);

        BarDataSet setQC = new BarDataSet(yValsQC, "QUALITY CONTROL");
        setQC.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        setQC.setColor(Color.parseColor("#60BFFA"));
        setQC.setDrawValues(true);
        setQC.setValueTextSize(8f);
        setQC.setValueFormatter(new PercentFormatter());


        List<BarDataSet> dataSets = new ArrayList<>();
        dataSets.add(setTQ);
        dataSets.add(setQA);
        dataSets.add(setQC);
        BarData data = new BarData(xVals, dataSets);

        qsChart.setData(data);
        qsChart.invalidate();

    }

}
