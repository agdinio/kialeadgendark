package com.almajid.kialeadgen.helpers;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by relly on 9/7/15.
 */
public class Common {

    public static void hideKeyboard(Context context) {

        FragmentActivity activity = (FragmentActivity)context;

        View softview = activity.getCurrentFocus();
        if (softview != null) {
            InputMethodManager im = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(softview.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
