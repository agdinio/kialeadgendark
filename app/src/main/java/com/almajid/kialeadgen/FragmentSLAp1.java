package com.almajid.kialeadgen;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.ClosingRate;
import com.almajid.kialeadgen.beans.ServiceLevelAgreement;
import com.almajid.kialeadgen.beans.VoiceOfCustomer;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentSLAp1 extends Fragment {


    private ServiceLevelAgreement sla;
    private View rootView;
    private LinearLayout content_frame_nav;
    private TextView textviewClosingRateTarget;
    private TextView textviewClosingRateAverage;
    private ImageView imageClosingRateArrow;
    private TextView textviewVOCRateTarget;
    private TextView textviewVOCRateAverage;
    private ImageView imageVOCArrow;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (sla != null) {
            rootView = inflater.inflate(R.layout.fragment_sla_p1, container, false);

            textviewClosingRateTarget = (TextView) rootView.findViewById(R.id.textviewClosingRateTarget);
            textviewClosingRateAverage = (TextView) rootView.findViewById(R.id.textviewClosingRateAverage);
            imageClosingRateArrow = (ImageView) rootView.findViewById(R.id.imageClosingRateArrow);

            textviewVOCRateTarget = (TextView) rootView.findViewById(R.id.textviewVOCRateTarget);
            textviewVOCRateAverage = (TextView) rootView.findViewById(R.id.textviewVOCRateAverage);
            imageVOCArrow = (ImageView) rootView.findViewById(R.id.imageVOCArrow);

            refresh();
        } else {
            Toast.makeText(getActivity().getBaseContext(), "Server Connection Error: Unable to connect.", Toast.LENGTH_LONG).show();
        }

/*
        content_frame_nav = (LinearLayout) ((FragmentActivity) rootView.getContext()).findViewById(R.id.content_frame_nav);
        content_frame_nav.setVisibility(View.GONE);
*/


        return rootView;
    }

    public void setSLA(ServiceLevelAgreement sla) {
        this.sla = sla;
    }

    public void refresh() {
        ClosingRate closingRate = sla.getClosingRate();

        if (closingRate == null) {
            Toast.makeText(getActivity().getBaseContext(), "Server Connection Error: Unable to connect.", Toast.LENGTH_LONG).show();
            return;
        }

        if (closingRate != null) {
            textviewClosingRateTarget.setText("TARGET: " + String.valueOf(closingRate.getTarget()) + "%");
            textviewClosingRateAverage.setText(String.valueOf(closingRate.getClosedWonPct()) + "%");
            if (closingRate.getTarget() > closingRate.getClosedWonPct() || closingRate.getClosedWonPct() == 0) {
                textviewClosingRateAverage.setBackgroundResource(R.drawable.base_rate_negative);
                imageClosingRateArrow.setImageResource(R.drawable.ic_arrow_indicator_negative);
            } else {
                textviewClosingRateAverage.setBackgroundResource(R.drawable.base_rate_positive);
                imageClosingRateArrow.setImageResource(R.drawable.ic_arrow_indicator_positive);
            }
        }

        VoiceOfCustomer voc = sla.getVoc();
        if (voc != null) {
            if (voc.getVocAverage() < 1) {
                textviewVOCRateTarget.setText("N.A.");
                textviewVOCRateAverage.setText("N.A.");
                textviewVOCRateAverage.setBackgroundResource(R.drawable.base_rate_negative);
                imageVOCArrow.setImageResource(R.drawable.ic_arrow_indicator_negative);
            } else {
                textviewVOCRateTarget.setText("TARGET: " + String.valueOf(voc.getVocTarget()) + "%");
                textviewVOCRateAverage.setText(String.valueOf(voc.getVocAverage()) + "%");
                if (voc.getVocTarget() > voc.getVocAverage()) {
                    textviewVOCRateAverage.setBackgroundResource(R.drawable.base_rate_negative);
                    imageVOCArrow.setImageResource(R.drawable.ic_arrow_indicator_negative);
                } else {
                    textviewVOCRateAverage.setBackgroundResource(R.drawable.base_rate_positive);
                    imageVOCArrow.setImageResource(R.drawable.ic_arrow_indicator_positive);
                }
            }
        }

    }

}
