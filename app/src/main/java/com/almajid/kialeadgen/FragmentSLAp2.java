package com.almajid.kialeadgen;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.ClosingRate;
import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.beans.ServiceLevelAgreement;
import com.almajid.kialeadgen.beans.VoiceOfCustomer;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.FillFormatter;
import com.github.mikephil.charting.utils.PercentFormatter;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentSLAp2 extends Fragment {


    private HorizontalBarChart leadsChart;
    private LineChart crChart;
    private BarChart vocChart;
    private View rootView;
    private ViewGroup container;
    private ServiceLevelAgreement sla;
    private TextView txtClosingRateTotal;
    private TextView txtClosingRateTotalCC;
    private TextView txtClosingRateTotalWI;
    private TextView txtClosingRateTotalPct;
    private TextView txtClosingRateTotalCCPct;
    private TextView txtClosingRateTotalWIPct;
    private TextView txtClosingRateAve;

    private TextView txtVocTodayPct;
    private TextView txtVocAverage;
    private LeadSharedPreferences prefs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.container = container;
        rootView = inflater.inflate(R.layout.fragment_sla_p2, container, false);
        leadsChart = (HorizontalBarChart) rootView.findViewById(R.id.leadsChart);
        crChart = (LineChart) rootView.findViewById(R.id.crChart);
        vocChart = (BarChart) rootView.findViewById(R.id.vocChart);

        txtClosingRateTotal = (TextView) rootView.findViewById(R.id.txtClosingRateTotal);
        txtClosingRateTotalCC = (TextView) rootView.findViewById(R.id.txtClosingRateTotalCC);
        txtClosingRateTotalWI = (TextView) rootView.findViewById(R.id.txtClosingRateTotalWI);
        txtClosingRateTotalPct = (TextView) rootView.findViewById(R.id.txtClosingRateTotalPct);
        txtClosingRateTotalCCPct = (TextView) rootView.findViewById(R.id.txtClosingRateTotalCCPct);
        txtClosingRateTotalWIPct = (TextView) rootView.findViewById(R.id.txtClosingRateTotalWIPct);
        txtClosingRateAve = (TextView) rootView.findViewById(R.id.txtClosingRateAve);

        txtVocTodayPct = (TextView) rootView.findViewById(R.id.txtVocTodayPct);
        txtVocAverage = (TextView) rootView.findViewById(R.id.txtVocAverage);

        return rootView;
    }

    public void setSLA(ServiceLevelAgreement sla) {
        this.sla = sla;
    }

    public void refreshChart() {
        crChart.resetTracking();
        crChart.setData(null);
        crChart.invalidate();

        vocChart.resetTracking();
        vocChart.setData(null);
        vocChart.invalidate();
    }

    public void updateLabels() {
        prefs = new LeadSharedPreferences(rootView.getContext());
        Filter filter = prefs.getFilter();
        if (filter == null) {
            return;
        }

        ClosingRate cr = sla.getClosingRate();

        txtClosingRateAve.setText(cr.getClosedWonPct() + "%");
        if (cr.getTarget() > cr.getClosedWonPct()) {
            txtClosingRateAve.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.rate_negative));
        } else {
            txtClosingRateAve.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.rate_positive));
        }

        txtClosingRateTotal.setText(String.valueOf(cr.getClosedWon()));
        txtClosingRateTotalCC.setText(String.valueOf(cr.getClosedWonCC()));
        txtClosingRateTotalWI.setText(String.valueOf(cr.getClosedWonWI()));
        txtClosingRateTotalPct.setText(String.valueOf(cr.getClosedWonPct()) + "%");
        txtClosingRateTotalCCPct.setText(String.valueOf(cr.getClosedWonCCPct()) + "%");
        txtClosingRateTotalWIPct.setText(String.valueOf(cr.getClosedWonWIPct()) + "%");

        VoiceOfCustomer voc = sla.getVoc();
        txtVocTodayPct.setText(String.valueOf(voc.getVocTodayPct()) + "%");
        txtVocAverage.setText(String.valueOf(voc.getVocAverage()) + "%");
        if (voc.getVocTarget() > voc.getVocAverage()) {
            txtVocAverage.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.rate_negative));
        } else {
            txtVocAverage.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.rate_positive));
        }

    }

    public void updateClosingRateChart() {

        prefs = new LeadSharedPreferences(rootView.getContext());
        Filter filter = prefs.getFilter();
        if (filter == null) {
            return;
        }

        ClosingRate cr = sla.getClosingRate();

        leadsChart.setDrawBarShadow(false);
        leadsChart.setDrawValueAboveBar(true);
        leadsChart.setDescription("");
        leadsChart.setPinchZoom(false);
        leadsChart.setDrawGridBackground(false);
        leadsChart.getLegend().setEnabled(false);
        leadsChart.getAxisLeft().setDrawLabels(false);
        leadsChart.animateY(2500);

        XAxis xl = leadsChart.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setDrawAxisLine(true);
        xl.setDrawGridLines(true);
        xl.setGridLineWidth(0.3f);
        xl.setTextColor(Color.WHITE);
        xl.setTextSize(8f);

        YAxis yr = leadsChart.getAxisRight();
        yr.setDrawAxisLine(true);
        yr.setDrawGridLines(false);
        yr.setTextColor(Color.WHITE);
        yr.setTextSize(8f);

        Legend l = leadsChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setFormSize(8f);
        l.setXEntrySpace(4f);


        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<BarEntry> values = new ArrayList<BarEntry>();


        String cra[] = TextUtils.isEmpty(cr.getClosedWonGraph()) ? new String[]{} : cr.getClosedWonGraph().split(",");



        if (filter.getFilterType().equalsIgnoreCase("MONTHLY")) {
            SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
            Calendar calFilterDate = Calendar.getInstance();
            Calendar calDateWithValue = Calendar.getInstance();
            Date filterdate = null;

            String curr_filters[] = filter.getMonthly().split(",");

            for (int i = 0; i < curr_filters.length; i++) {
                try {
                    filterdate = df.parse(curr_filters[i]);
                    calFilterDate.setTime(filterdate);

                    String str_month = Global.months[calFilterDate.get(Calendar.MONTH)];

                    xVals.add(str_month + " " + calFilterDate.get(Calendar.YEAR));
                    values.add(new BarEntry(Float.parseFloat("0"), i));

                    for (int j = 0; j < cra.length; j++) {


                        String ar[] = cra[j].split("=");

                        Date datewithvalue = df.parse(ar[0]);
                        calDateWithValue.setTime(datewithvalue);

                        if (calDateWithValue.get(Calendar.MONTH) == calFilterDate.get(Calendar.MONTH)
                                && calDateWithValue.get(Calendar.YEAR) == calFilterDate.get(Calendar.YEAR)) {
                            float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                            values.set(i, new BarEntry(e, i));
                        }

                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

        } else if (filter.getFilterType().equalsIgnoreCase("DATERANGE")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM-dd-''yy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            String curr_filters[] = filter.getDateRange().split(",");

            try {
                Date sdate = dffrom.parse(curr_filters[0]);
                Date edate = dfto.parse(curr_filters[1]);
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new BarEntry( Float.parseFloat("0"), i));

                for (int j=0; j<cra.length; j++) {

                    String ar[] = cra[j].split("=");

                    if (ar[0].equalsIgnoreCase((dfto.format(targetday)))) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new BarEntry(e, i));
                    }
                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        } else if (filter.getFilterType().equalsIgnoreCase("MTD")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-01-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM-dd-''yy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            try {
                Date sdate = dffrom.parse(dffrom.format(new Date()));
                Date edate = dfto.parse(dfto.format(new Date()));
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new BarEntry( Float.parseFloat("0"), i));

                for (int j=0; j<cra.length; j++) {


                    String ar[] = cra[j].split("=");

                    if (ar[0].equalsIgnoreCase(dfto.format(targetday))) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new BarEntry(e, i));
                    }

                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        }



        BarDataSet set1 = new BarDataSet(values, "");
        set1.setColor(Color.YELLOW);
        set1.setValueTextColor(Color.WHITE);
        set1.setValueFormatter(new PercentFormatter());


        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(set1);

        BarData data = new BarData(xVals, dataSets);
        data.setValueTextSize(8f);

        leadsChart.setData(data);

    }

    public void updateClosingRateChartLine() {

        prefs = new LeadSharedPreferences(rootView.getContext());
        Filter filter = prefs.getFilter();
        if (filter == null) {
            return;
        }
        ClosingRate cr = sla.getClosingRate();


        txtClosingRateAve.setText(cr.getClosedWonPct() + "%");
        txtClosingRateTotal.setText(String.valueOf(cr.getClosedWon()));
        txtClosingRateTotalCC.setText(String.valueOf(cr.getClosedWonCC()));
        txtClosingRateTotalWI.setText(String.valueOf(cr.getClosedWonWI()));
        txtClosingRateTotalPct.setText(String.valueOf(cr.getClosedWonPct()) + "%");
        txtClosingRateTotalCCPct.setText(String.valueOf(cr.getClosedWonCCPct()) + "%");
        txtClosingRateTotalWIPct.setText(String.valueOf(cr.getClosedWonWIPct()) + "%");

        VoiceOfCustomer voc = sla.getVoc();
        txtVocTodayPct.setText(String.valueOf(voc.getVocTodayPct()) + "%");
        txtVocAverage.setText(String.valueOf(voc.getVocAverage()) + "%");

        Legend l = crChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));

        crChart.setDrawGridBackground(false);
        crChart.setDescription("");
        //crChart.setHighlightEnabled(false);
        //crChart.setTouchEnabled(false);
        //crChart.setScaleEnabled(true);
        crChart.setPinchZoom(false);

        crChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
        crChart.resetTracking();
        crChart.getAxisRight().setDrawLabels(false);
        crChart.getAxisLeft().setDrawLabels(false);
        crChart.getLegend().setEnabled(false);

        String cra[] = TextUtils.isEmpty(cr.getClosedWonGraph()) ? new String[]{} : cr.getClosedWonGraph().split(",");

        List<String> xVals = new ArrayList<>();
        List<Entry> values = new ArrayList<>();


        if (filter.getFilterType().equalsIgnoreCase("MONTHLY")) {
            String curr_filters[] = filter.getMonthly().split(",");
            for (int i=0; i<curr_filters.length; i++) {
                String month = curr_filters[i];
                String str_month = Global.months[Integer.parseInt(month)-1];

                xVals.add(str_month);
                values.add(new Entry( Float.parseFloat("0"), i));

                for (int j=0; j<cra.length; j++) {


                    String ar[] = cra[j].split("=");

                    if (ar[0].equals(month)) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new Entry(e, i));
                    }

                }
            }

        } else if (filter.getFilterType().equalsIgnoreCase("DATERANGE")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM/dd/yyyy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            String curr_filters[] = filter.getDateRange().split(",");

            try {
                Date sdate = dffrom.parse(curr_filters[0]);
                Date edate = dfto.parse(curr_filters[1]);
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new Entry( Float.parseFloat("0"), i));

                for (int j=0; j<cra.length; j++) {

                    String ar[] = cra[j].split("=");

                    if (ar[0].equalsIgnoreCase((dfto.format(targetday)))) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new Entry(e, i));
                    }
                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        } else if (filter.getFilterType().equalsIgnoreCase("MTD")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-01-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM/dd/yyyy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            try {
                Date sdate = dffrom.parse(dffrom.format(new Date()));
                Date edate = dfto.parse(dfto.format(new Date()));
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new Entry( Float.parseFloat("0"), i));

                for (int j=0; j<cra.length; j++) {


                    String ar[] = cra[j].split("=");

                    if (ar[0].equalsIgnoreCase(dfto.format(targetday))) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new Entry(e, i));
                    }

                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        }


        XAxis xaxis = crChart.getXAxis();
        xaxis.setTextSize(8f);
        xaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        xaxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis lyaxis = crChart.getAxisLeft();
        lyaxis.setTextSize(8f);
        lyaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        //lyaxis.setAxisMaxValue(15f);

        LineDataSet d = new LineDataSet(values, "Closing Rate");
        d.setValueTextSize(8f);
        d.setColor(Color.YELLOW);
        d.setDrawCircles(false);
        d.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        d.setValueFormatter(new PercentFormatter());
        LineData data = new LineData(xVals, d);

        crChart.setData(data);
        crChart.invalidate();

    }

    public void updateVOCChart() {

        prefs = new LeadSharedPreferences(rootView.getContext());
        Filter filter = prefs.getFilter();
        if (filter == null) {
            return;
        }
        VoiceOfCustomer voc = sla.getVoc();

        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<BarEntry> values = new ArrayList<BarEntry>();


        String voca[] = TextUtils.isEmpty(voc.getVocGraph()) ? new String[]{} : voc.getVocGraph().split(",");

        if (filter.getFilterType().equalsIgnoreCase("MONTHLY")) {
            SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
            Calendar calFilterDate = Calendar.getInstance();
            Calendar calDateWithValue = Calendar.getInstance();
            Date filterdate = null;

            String curr_filters[] = filter.getMonthly().split(",");

            for (int i = 0; i < curr_filters.length; i++) {
                try {
                    filterdate = df.parse(curr_filters[i]);
                    calFilterDate.setTime(filterdate);

                    String str_month = Global.months[calFilterDate.get(Calendar.MONTH)];

                    xVals.add(str_month + " " + calFilterDate.get(Calendar.YEAR));
                    values.add(new BarEntry(Float.parseFloat("0"), i));

                    for (int j = 0; j < voca.length; j++) {


                        String ar[] = voca[j].split("=");

                        Date datewithvalue = df.parse(ar[0]);
                        calDateWithValue.setTime(datewithvalue);

                        if (calDateWithValue.get(Calendar.MONTH) == calFilterDate.get(Calendar.MONTH)
                                && calDateWithValue.get(Calendar.YEAR) == calFilterDate.get(Calendar.YEAR)) {
                            float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                            values.set(i, new BarEntry(e, i));
                        }

                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

        } else if (filter.getFilterType().equalsIgnoreCase("DATERANGE")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM-dd-''yy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            String curr_filters[] = filter.getDateRange().split(",");

            try {
                Date sdate = dffrom.parse(curr_filters[0]);
                Date edate = dfto.parse(curr_filters[1]);
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new BarEntry( Float.parseFloat("0"), i));

                for (int j=0; j<voca.length; j++) {

                    String ar[] = voca[j].split("=");

                    if (ar[0].equalsIgnoreCase((dfto.format(targetday)))) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new BarEntry(e, i));
                    }
                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        } else if (filter.getFilterType().equalsIgnoreCase("MTD")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-01-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM-dd-''yy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            try {
                Date sdate = dffrom.parse(dffrom.format(new Date()));
                Date edate = dfto.parse(dfto.format(new Date()));
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new BarEntry( Float.parseFloat("0"), i));

                for (int j=0; j<voca.length; j++) {


                    String ar[] = voca[j].split("=");

                    if (ar[0].equalsIgnoreCase(dfto.format(targetday))) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new BarEntry(e, i));
                    }

                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        }


        vocChart.setDescription("");
        vocChart.setPinchZoom(false);
        vocChart.setDrawBarShadow(false);
        vocChart.setDrawGridBackground(false);
        vocChart.getAxisLeft().setDrawGridLines(false);
        vocChart.animateY(2500);
        vocChart.getAxisRight().setDrawLabels(false);
        vocChart.getAxisLeft().setDrawLabels(false);
        vocChart.getLegend().setEnabled(false);

        XAxis xAxis = vocChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(8f);
        xAxis.setSpaceBetweenLabels(0);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));

        YAxis lyaxis = vocChart.getAxisLeft();
        lyaxis.setTextSize(8f);
        lyaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));

/*
        int xxcounter = 10;
        int yycounter = 100;

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

        for (int i = 0; i < xxcounter + 1; i++) {
            float mult = (yycounter + 1);
            float val1 = (float) (Math.random() * mult) + mult / 3;
            yVals1.add(new BarEntry((int) val1, i));
        }

        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < xxcounter + 1; i++) {
            xVals.add((int) yVals1.get(i).getVal() + "");
        }

        BarDataSet set1 = new BarDataSet(yVals1, "Data Set");
        set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
        set1.setDrawValues(false);

        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(set1);
*/

//        ArrayList<String> xVals = new ArrayList<>();
//        xVals.add("Jan");
//        xVals.add("Feb");
//        xVals.add("Mar");
//        xVals.add("Apr");
//        xVals.add("May");
//        xVals.add("Jun");
//        xVals.add("Jul");
//        xVals.add("Aug");
//        xVals.add("Sep");
//        xVals.add("Oct");
//        xVals.add("Nov");
//        xVals.add("Dec");

//        ArrayList<BarEntry> yVals = new ArrayList<>();
//        BarEntry barEntry = new BarEntry(3, 0);
//        yVals.add(barEntry);
//
//        barEntry = new BarEntry(2, 1);
//        yVals.add(barEntry);
//        barEntry = new BarEntry(1, 2);
//        yVals.add(barEntry);
//        barEntry = new BarEntry(4, 3);
//        yVals.add(barEntry);
//        barEntry = new BarEntry(5, 4);
//        yVals.add(barEntry);
//        barEntry = new BarEntry((float)4.5, 5);
//        yVals.add(barEntry);
//        barEntry = new BarEntry((float)3.2, 6);
//        yVals.add(barEntry);
//        barEntry = new BarEntry(7, 7);
//        yVals.add(barEntry);
//        barEntry = new BarEntry(4, 8);
//        yVals.add(barEntry);
//        barEntry = new BarEntry((float)6.8, 9);
//        yVals.add(barEntry);
//        barEntry = new BarEntry(2, 10);
//        yVals.add(barEntry);
//        barEntry = new BarEntry((float)3.5, 11);
//        yVals.add(barEntry);

        BarDataSet barDataSet = new BarDataSet(values, "VOC");
        barDataSet.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        barDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        barDataSet.setDrawValues(true);
        barDataSet.setValueTextSize(8f);
        barDataSet.setValueFormatter(new PercentFormatter());

        BarData data = new BarData(xVals, barDataSet);

        vocChart.setData(data);
        vocChart.invalidate();

    }


}
