package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.TestDrive;
import com.almajid.kialeadgen.beans.TestDriveLeads;
import com.almajid.kialeadgen.beans.TestDriveLeadsStatus;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentLeadsp1 extends Fragment {

    private TestDriveLeads leads;
    private View rootView;
    private PieChart leadsChart;
    private TextView txtOpen;
    private TextView txtWon;
    private TextView txtLoss;
    private TextView txtDeadlead;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (leads != null) {
            rootView = inflater.inflate(R.layout.fragment_leads_p1, container, false);
            leadsChart = (PieChart) rootView.findViewById(R.id.leadsChart);
            txtOpen = (TextView) rootView.findViewById(R.id.txtOpen);
            txtWon = (TextView) rootView.findViewById(R.id.txtWon);
            txtLoss = (TextView) rootView.findViewById(R.id.txtLoss);
            txtDeadlead = (TextView) rootView.findViewById(R.id.txtDeadlead);

            updateChart();
        } else {
            Toast.makeText(getActivity().getBaseContext(), "Server Connection Error: Unable to connect.", Toast.LENGTH_LONG).show();
        }


        return rootView;
    }

    public void setLeads(TestDriveLeads leads) {
        this.leads = leads;
    }

    public void updateChart() {

        TestDriveLeadsStatus status = leads.getTestDriveLeadsStatus();

        if (status == null) {
            Toast.makeText(getActivity().getBaseContext(), "Server Connection Error: Unable to connect.", Toast.LENGTH_LONG).show();
            return;
        }

        txtOpen.setText(String.valueOf(status.getOpenpct()) + "%");
        txtWon.setText(String.valueOf(status.getWonpct()) + "%");
        txtLoss.setText(String.valueOf(status.getLosspct()) + "%");
        txtDeadlead.setText(String.valueOf(status.getDeadleadpct()) + "%");


        leadsChart.setUsePercentValues(true);
        leadsChart.setDescription("");
        leadsChart.setDragDecelerationFrictionCoef(0.95f);

        leadsChart.setDrawHoleEnabled(true);
        leadsChart.setHoleColorTransparent(true);

        leadsChart.setTransparentCircleColor(Color.WHITE);
        leadsChart.setTransparentCircleAlpha(50);

        leadsChart.setHoleRadius(50f);
        leadsChart.setTransparentCircleRadius(55f);

        leadsChart.setDrawCenterText(true);
        leadsChart.setDrawSliceText(false);

        leadsChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        leadsChart.setRotationEnabled(false);
        leadsChart.animateY(1500, Easing.EasingOption.EaseInOutQuad);

        Legend l = leadsChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_CENTER);
        l.setTextColor(Color.WHITE);
        l.setTextSize(8f);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);


        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        yVals1.add(new Entry(status.getOpenpct(), 0));
        yVals1.add(new Entry(status.getWonpct(), 1));
        yVals1.add(new Entry(status.getLosspct(), 2));
        yVals1.add(new Entry(status.getDeadleadpct(), 3));


        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("OPEN");
        xVals.add("CLOSE WON");
        xVals.add("CLOSE LOSS");
        xVals.add("DEAD LEAD");

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(5f);



        ArrayList<Integer> colors = new ArrayList<Integer>();
        colors.add(Color.parseColor("#162d46"));
        colors.add(Color.parseColor("#354F30"));
        colors.add(Color.parseColor("#244954"));
        colors.add(Color.parseColor("#461C1F"));

//        for (int c : ColorTemplate.VORDIPLOM_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.JOYFUL_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.COLORFUL_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.LIBERTY_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.PASTEL_COLORS)
//            colors.add(c);
//
//        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(8f);
        data.setValueTextColor(Color.WHITE);

        leadsChart.setData(data);

        // undo all highlights
        leadsChart.highlightValues(null);

        leadsChart.invalidate();

    }

}
