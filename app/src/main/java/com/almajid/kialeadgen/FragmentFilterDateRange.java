package com.almajid.kialeadgen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.squareup.timessquare.CalendarPickerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by relly on 9/7/15.
 */
public class FragmentFilterDateRange extends Fragment {

    private View rootView;
    private CalendarPickerView calendar;
    private LeadSharedPreferences prefs;
    private Button btnSave;
    private int dateRangeLimit;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dateRangeLimit = 30;

        rootView = inflater.inflate(R.layout.fragment_filter_daterange, container, false);

        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);

        final Calendar lastMonth = Calendar.getInstance();
        lastMonth.add(Calendar.MONTH, -1);

        final Calendar minMonth = Calendar.getInstance();
        SimpleDateFormat dateformatter = new SimpleDateFormat("MM-dd-yyyy");
        try {
            minMonth.setTime(dateformatter.parse("10-01-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final Calendar thisMonth = Calendar.getInstance();

        btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(onclickSave);

        calendar = (CalendarPickerView) rootView.findViewById(R.id.calendar_view);
        calendar.init(minMonth.getTime(), thisMonth.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
        calendar.scrollToDate(thisMonth.getTime());

        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                List<Date> dates = calendar.getSelectedDates();
                if (dates.size() > dateRangeLimit) {
                    new AlertDialog.Builder(rootView.getContext())
                            .setCancelable(false)
                            .setTitle("Invalid")
                            .setMessage("Date range should not exceed " + dateRangeLimit + " consecutive days")
                            .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .create()
                            .show();
                }

            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });



//        done = (Button) rootView.findViewById(R.id.btnDone);
//        done.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd");
//
//                String toast = "Selected: " + dateformatter.format(calendar.getSelectedDate());
//                Toast.makeText(rootView.getContext(), toast, LENGTH_SHORT).show();
//            }
//        });

        return rootView;
    }


    private View.OnClickListener onclickSave = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (calendar.getSelectedDates().size() > 0) {

                if (calendar.getSelectedDates().size() > dateRangeLimit) {
                    new AlertDialog.Builder(rootView.getContext())
                            .setCancelable(false)
                            .setTitle("Invalid")
                            .setMessage("Date range should not exceed " + dateRangeLimit + " consecutive days")
                            .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .create()
                            .show();
                    return;
                }


                SimpleDateFormat dateformatter = new SimpleDateFormat("MM-dd-yyyy");

                StringBuilder dates = new StringBuilder();

                if (calendar.getSelectedDates().size() == 1) {
                    Date d = calendar.getSelectedDates().get(0);
                    String s = dateformatter.format(d);

                    dates.append(s + "," + s + ",");
                } else {
                    for (int i=0; i<calendar.getSelectedDates().size(); i++) {
                        Date d = calendar.getSelectedDates().get(i);
                        String s = dateformatter.format(d);

                        if (i == 0) {
                            dates.append(s + ",");
                        }
                        if ((i>0) && (i==calendar.getSelectedDates().size() - 1)) {
                            dates.append(s + ",");
                        }
                    }
                }

                dates.setLength(dates.length() - 1);
                prefs = new LeadSharedPreferences(rootView.getContext());
                Filter filter = prefs.getFilter();
                if (filter == null) {

                    filter = new Filter();
                    filter.setVoice(false);
                    filter.setEmail(false);
                    filter.setLiveChat(false);
                    filter.setSocialMedia(false);
                    filter.setWalkin(false);
                    filter.setAll(true);
                    filter.setIndividual(true);
                    filter.setLuxury(true);

                    filter.setDateRange(dates.toString());
                    filter.setMonthly("");
                    filter.setMtd(false);
                    filter.setFilterType("DATERANGE");

                    String jsonString = prefs.toJson(filter);
                    prefs.editMode();
                    prefs.putString("FILTER", jsonString);
                    prefs.commitMode();
                } else {

                    filter.setDateRange(dates.toString());
                    filter.setMonthly("");
                    filter.setMtd(false);
                    filter.setFilterType("DATERANGE");

                    String jsonString = prefs.toJson(filter);
                    prefs.editMode();
                    prefs.putString("FILTER", jsonString);
                    prefs.commitMode();
                }

                new AlertDialog.Builder(rootView.getContext())
                        .setCancelable(false)
                        .setTitle("Saved")
                        .setMessage("DATE-RANGE HAS BEEN SET")
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .create()
                        .show();
            } else {

                new AlertDialog.Builder(rootView.getContext())
                        .setCancelable(false)
                        .setTitle("No Date Range")
                        .setMessage("PLEASE SELECT A DATE RANGE")
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .create()
                        .show();

            }

        }
    };




}
