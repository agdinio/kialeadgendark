package com.almajid.kialeadgen.beans;

import android.text.TextUtils;

/**
 * Created by relly on 10/7/15.
 */
public class VoiceOfCustomer {

    private int actualVocScore;
    private int MaxVocScore;
    private int VocTarget;
    private int VocAverage;
    private int vocTodayPct;
    private String vocGraph;

    public int getActualVocScore() {
        return actualVocScore;
    }

    public void setActualVocScore(int actualVocScore) {
        this.actualVocScore = actualVocScore;
    }

    public int getMaxVocScore() {
        return MaxVocScore;
    }

    public void setMaxVocScore(int maxVocScore) {
        MaxVocScore = maxVocScore;
    }

    public int getVocTarget() {
        return VocTarget;
    }

    public void setVocTarget(int vocTarget) {
        VocTarget = vocTarget;
    }

    public int getVocAverage() {
        return VocAverage;
    }

    public void setVocAverage(int vocAverage) {
        VocAverage = vocAverage;
    }

    public int getVocTodayPct() {
        return vocTodayPct;
    }

    public void setVocTodayPct(int vocTodayPct) {
        this.vocTodayPct = vocTodayPct;
    }

    public String getVocGraph() {
        return vocGraph;
    }

    public void setVocGraph(String vocGraph) {
        this.vocGraph = vocGraph;
    }
}
