package com.almajid.kialeadgen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.services.InitTask;
import com.almajid.kialeadgen.services.ServiceUserAuth;


/**
 * Created by relly on 9/6/15.
 */
public class LoginActivity extends Activity {

    private LeadSharedPreferences prefs;
    private Context context = this;
    private ProgressDialog dialog;
    private InitTask initTask;
    private EditText txtUsername;
    private EditText txtPassword;
    private Button btnLogin;
    private TextView lblRegister;
    private TextView lblForgot;
    private CheckBox chkRememberMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        chkRememberMe = (CheckBox) findViewById(R.id.chkRememberMe);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(loginClickListener);

        prefs = new LeadSharedPreferences(context);
        if (prefs.getUser() != null && prefs.isRememberMe()) {
            Intent mainIntent = new Intent(this, MainActivity.class);
            startActivity(mainIntent);
        }
//        lblForgot = (TextView) findViewById(R.id.lblForgot);
//        lblForgot.setOnClickListener(forgotPassClickListener);

//        lblRegister = (TextView) findViewById(R.id.lblRegister);
//        lblRegister.setOnClickListener(registerClickListener);

    }


    private View.OnClickListener loginClickListener = new View.OnClickListener() {
        @SuppressLint("NewApi")
        @Override
        public void onClick(View v) {
            if (txtUsername.getText().toString().isEmpty()) {
                Toast.makeText(getApplicationContext(),
                        "Please Enter a User Name", Toast.LENGTH_SHORT)
                        .show();
                return;
            } else if (txtPassword.getText().toString().isEmpty()) {
                Toast.makeText(getApplicationContext(),
                        "Please Enter a Password", Toast.LENGTH_SHORT)
                        .show();
                return;
            }

            initTask = new InitTask();
            initTask.execute(context);

            hideKeyboard();

            dialog  = ProgressDialog.show(context, "", "Signing in...");

            Object[] params = { dialog,
                    txtUsername.getText().toString(),
                    txtPassword.getText().toString(),
                    chkRememberMe.isChecked() };
            ServiceUserAuth thread = new ServiceUserAuth(context, params);
            thread.start();


//            dialog  = ProgressDialog.show(context, "", "Signing in...");
//
//            Object[] params = { dialog,
//                    txtUsername.getText().toString(),
//                    txtPassword.getText().toString(),
//                    chkRememberMe.isChecked() };
//            ServiceUserAuth thread = new ServiceUserAuth(context, params);
//            thread.start();
        }
    };

    private void hideKeyboard() {
        View softview = getCurrentFocus();
        if (softview != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(softview.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);

    }

}
