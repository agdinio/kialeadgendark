package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentLeadsDailyTestDrive extends Fragment {


    private View rootView;
    private LineChart leadsChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_leads_daily_test_drive, container, false);
        leadsChart = (LineChart) rootView.findViewById(R.id.leadsChart);

        updateChart();

        return rootView;
    }


    public void updateChart() {

        leadsChart.resetTracking();
        leadsChart.setDrawGridBackground(false);
        leadsChart.setDescription("");
        //crChart.setHighlightEnabled(false);
        //crChart.setTouchEnabled(false);
        //crChart.setScaleEnabled(true);
        leadsChart.setPinchZoom(false);

        leadsChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
        leadsChart.resetTracking();
        leadsChart.getAxisRight().setDrawLabels(false);
        leadsChart.getAxisLeft().setDrawLabels(false);
        leadsChart.getLegend().setEnabled(false);

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("1" + "");
        xVals.add("2" + "");
        xVals.add("3" + "");
        xVals.add("4" + "");
        xVals.add("5" + "");
        xVals.add("6" + "");
        xVals.add("7" + "");
        xVals.add("8" + "");
        xVals.add("9" + "");
        xVals.add("10" + "");

        ArrayList<Entry> values = new ArrayList<Entry>();
        values.add(new Entry((float) 3, 0));
        values.add(new Entry((float) 4.3, 1));
        values.add(new Entry((float) 5.7, 2));
        values.add(new Entry((float) 3.1, 3));
        values.add(new Entry((float) 5, 4));
        values.add(new Entry((float) 6, 5));
        values.add(new Entry((float) 8.9, 6));
        values.add(new Entry((float) 7, 7));
        values.add(new Entry((float) 8, 8));
        values.add(new Entry((float) 5, 9));

        XAxis xaxis = leadsChart.getXAxis();
        xaxis.setTextSize(8f);
        xaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        xaxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis lyaxis = leadsChart.getAxisLeft();
        lyaxis.setTextSize(8f);
        lyaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        lyaxis.setAxisMaxValue(15f);

        LineDataSet d = new LineDataSet(values, "Sample");
        d.setValueTextSize(7f);
        d.setColor(Color.YELLOW);
        d.setDrawCircles(false);
        d.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        d.setValueFormatter(new PercentFormatter());
        LineData data = new LineData(xVals, d);

        leadsChart.setData(data);
        leadsChart.invalidate();

    }


}
