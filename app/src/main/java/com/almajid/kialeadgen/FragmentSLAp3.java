package com.almajid.kialeadgen;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.almajid.kialeadgen.beans.AttendanceRate;
import com.almajid.kialeadgen.beans.QualificationRate;
import com.almajid.kialeadgen.beans.ServiceLevelAgreement;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentSLAp3 extends Fragment {


    private View rootView;
    private LinearLayout content_frame_nav;
    private ServiceLevelAgreement sla;

    private TextView textviewAttRateTarget;
    private TextView textviewAttRatePct;
    private ImageView imageAttRateArrow;

    private TextView textviewQualRateTarget;
    private TextView textviewQualRatePct;
    private ImageView imageQualRateArrow;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_sla_p3, container, false);


/*
        content_frame_nav = (LinearLayout) ((FragmentActivity) rootView.getContext()).findViewById(R.id.content_frame_nav);
        content_frame_nav.setVisibility(View.GONE);
*/

        textviewAttRateTarget = (TextView) rootView.findViewById(R.id.textviewAttRateTarget);
        textviewAttRatePct = (TextView) rootView.findViewById(R.id.textviewAttRatePct);
        imageAttRateArrow = (ImageView) rootView.findViewById(R.id.imageAttRateArrow);

        textviewQualRateTarget = (TextView) rootView.findViewById(R.id.textviewQualRateTarget);
        textviewQualRatePct = (TextView) rootView.findViewById(R.id.textviewQualRatePct);
        imageQualRateArrow = (ImageView) rootView.findViewById(R.id.imageQualRateArrow);

        return rootView;
    }

    public void setSLA(ServiceLevelAgreement sla) {
        this.sla = sla;
    }

    public void refresh() {
        AttendanceRate att = sla.getAttendanceRate();
        if (att != null) {
            textviewAttRateTarget.setText("TARGET: " + String.valueOf(att.getAttendedTarget()) + "%");
            textviewAttRatePct.setText(String.valueOf(att.getAttendedCCPct()) + "%");
            if (att.getAttendedTarget() > att.getAttendedCCPct() || att.getAttendedCCPct() == 0) {
                textviewAttRatePct.setBackgroundResource(R.drawable.base_rate_negative);
                imageAttRateArrow.setImageResource(R.drawable.ic_arrow_indicator_negative);
            } else {
                textviewAttRatePct.setBackgroundResource(R.drawable.base_rate_positive);
                imageAttRateArrow.setImageResource(R.drawable.ic_arrow_indicator_positive);
            }
        }

        QualificationRate qual = sla.getQualificationRate();
        if (qual != null) {
            textviewQualRateTarget.setText("TARGET: " + String.valueOf(qual.getQualificationTarget()) + "%");
            textviewQualRatePct.setText(String.valueOf(qual.getQualifiedPct()) + "%");
            if (qual.getQualificationTarget() > qual.getQualifiedPct() || qual.getQualifiedPct() == 0) {
                textviewQualRatePct.setBackgroundResource(R.drawable.base_rate_negative);
                imageQualRateArrow.setImageResource(R.drawable.ic_arrow_indicator_negative);
            } else {
                textviewQualRatePct.setBackgroundResource(R.drawable.base_rate_positive);
                imageQualRateArrow.setImageResource(R.drawable.ic_arrow_indicator_positive);
            }
        }
    }

}
