package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.TestDriveLeads;
import com.almajid.kialeadgen.beans.TestDriveLeadsReasons;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentLeadsp3 extends Fragment {


    private TestDriveLeads leads;
    private View rootView;
    private HorizontalBarChart reasonChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (leads != null) {
            rootView = inflater.inflate(R.layout.fragment_leads_p3, container, false);
            reasonChart = (HorizontalBarChart) rootView.findViewById(R.id.reasonChart);

        } else {
            Toast.makeText(getActivity().getBaseContext(), "Data cannot be retrieved. Please try again.", Toast.LENGTH_LONG).show();
        }


        return rootView;
    }

    public void setLeads(TestDriveLeads leads) {
        this.leads = leads;
    }

    public void updateChart() {

        List<TestDriveLeadsReasons> list = leads.getReasonsList();

        if (list != null && list.size() > 0) {

            ArrayList<String> xVals = new ArrayList<String>();
            ArrayList<BarEntry> values = new ArrayList<BarEntry>();

            for (int i=0; i<list.size(); i++) {
                TestDriveLeadsReasons reason = list.get(i);
                xVals.add(reason.getName());
                values.add(new BarEntry(reason.getTotal(), i));
            }

            setCharValue(xVals, values);
        }

    }

    private void setCharValue(ArrayList<String> xVals, ArrayList<BarEntry> values) {

        reasonChart.setDrawBarShadow(false);
        reasonChart.setDrawValueAboveBar(true);
        reasonChart.setDescription("");
        reasonChart.setPinchZoom(false);
        reasonChart.setDrawGridBackground(false);
        reasonChart.getLegend().setEnabled(false);
        reasonChart.getAxisLeft().setDrawLabels(false);
        reasonChart.animateY(2500);

        XAxis xl = reasonChart.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setDrawAxisLine(true);
        xl.setDrawGridLines(true);
        xl.setGridLineWidth(0.3f);
        xl.setTextColor(Color.WHITE);
        xl.setTextSize(8f);

        YAxis yr = reasonChart.getAxisRight();
        yr.setDrawAxisLine(true);
        yr.setDrawGridLines(false);
        yr.setTextColor(Color.WHITE);
        yr.setTextSize(8f);

        Legend l = reasonChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setFormSize(8f);
        l.setXEntrySpace(4f);

        BarDataSet set1 = new BarDataSet(values, "Close Loss Reasons");
        set1.setColor(Color.YELLOW);
        set1.setValueTextColor(Color.WHITE);

        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(set1);

        BarData data = new BarData(xVals, dataSets);
        data.setValueTextSize(8f);

        reasonChart.setData(data);

    }

}
