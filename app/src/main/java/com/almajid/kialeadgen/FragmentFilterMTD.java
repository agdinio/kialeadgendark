package com.almajid.kialeadgen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentFilterMTD extends Fragment {

    private LeadSharedPreferences prefs;

    private View rootView;
    private TextView txtMTD;
    private Button btnSave;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_filter_mtd, container, false);

        SimpleDateFormat dffrom = new SimpleDateFormat("yyyy-MM-01");
        SimpleDateFormat dfto = new SimpleDateFormat("yyyy-MM-dd");

        txtMTD = (TextView) rootView.findViewById(R.id.txtMTD);
        txtMTD.setText(dffrom.format(new Date()) + " to " + dfto.format(new Date()));

        btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prefs = new LeadSharedPreferences(rootView.getContext());
                Filter filter = prefs.getFilter();
                if (filter == null) {

                    filter = new Filter();
                    filter.setVoice(false);
                    filter.setEmail(false);
                    filter.setLiveChat(false);
                    filter.setSocialMedia(false);
                    filter.setWalkin(false);
                    filter.setAll(true);
                    filter.setIndividual(true);
                    filter.setLuxury(true);

                    filter.setDateRange("");
                    filter.setMonthly("");
                    filter.setMtd(true);
                    filter.setFilterType("MTD");

                    String jsonString = prefs.toJson(filter);
                    prefs.editMode();
                    prefs.putString("FILTER", jsonString);
                    prefs.commitMode();
                } else {

                    filter.setDateRange("");
                    filter.setMonthly("");
                    filter.setMtd(true);
                    filter.setFilterType("MTD");

                    String jsonString = prefs.toJson(filter);
                    prefs.editMode();
                    prefs.putString("FILTER", jsonString);
                    prefs.commitMode();
                }

                new AlertDialog.Builder(rootView.getContext())
                        .setCancelable(false)
                        .setTitle("Saved")
                        .setMessage("MONTH-TO-DATE HAS BEEN SET")
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .create()
                        .show();


            }
        });

        return rootView;
    }



}
