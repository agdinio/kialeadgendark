package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 11/15/15.
 */
public class QualificationRate {

    private int qualificationTarget;
    private int qualified;
    private int qualifiedPct;
    private int nonQualified;
    private int qualifiedTodayPct;
    private String qualifiedGraph;

    public int getQualificationTarget() {
        return qualificationTarget;
    }

    public void setQualificationTarget(int qualificationTarget) {
        this.qualificationTarget = qualificationTarget;
    }

    public int getQualified() {
        return qualified;
    }

    public void setQualified(int qualified) {
        this.qualified = qualified;
    }

    public int getQualifiedPct() {
        return qualifiedPct;
    }

    public void setQualifiedPct(int qualifiedPct) {
        this.qualifiedPct = qualifiedPct;
    }

    public int getNonQualified() {
        return nonQualified;
    }

    public void setNonQualified(int nonQualified) {
        this.nonQualified = nonQualified;
    }

    public String getQualifiedGraph() {
        return qualifiedGraph;
    }

    public void setQualifiedGraph(String qualifiedGraph) {
        this.qualifiedGraph = qualifiedGraph;
    }

    public int getQualifiedTodayPct() {
        return qualifiedTodayPct;
    }

    public void setQualifiedTodayPct(int qualifiedTodayPct) {
        this.qualifiedTodayPct = qualifiedTodayPct;
    }
}
