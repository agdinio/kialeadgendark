package com.almajid.kialeadgen.beans;

import java.util.List;

/**
 * Created by relly on 11/23/15.
 */
public class TestDriveLeads {

    private TestDriveLeadsStatus testDriveLeadsStatus;
    private List<TestDriveLeadsReasons> reasonsList;

    public TestDriveLeadsStatus getTestDriveLeadsStatus() {
        return testDriveLeadsStatus;
    }

    public void setTestDriveLeadsStatus(TestDriveLeadsStatus testDriveLeadsStatus) {
        this.testDriveLeadsStatus = testDriveLeadsStatus;
    }

    public List<TestDriveLeadsReasons> getReasonsList() {
        return reasonsList;
    }

    public void setReasonsList(List<TestDriveLeadsReasons> reasonsList) {
        this.reasonsList = reasonsList;
    }
}
