package com.almajid.kialeadgen;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.ClosingRate;
import com.almajid.kialeadgen.beans.Quality;
import com.almajid.kialeadgen.beans.ServiceLevelAgreement;
import com.almajid.kialeadgen.beans.VoiceOfCustomer;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentSLAp5 extends Fragment {


    private ServiceLevelAgreement sla;
    private View rootView;
    private LinearLayout content_frame_nav;
    private TextView txtQATarget;
    private TextView txtQAPct;
    private TextView txtQCTarget;
    private TextView txtQCPct;
    private TextView txtQTotalTarget;
    private TextView txtQTotalPct;

    private ImageView imageQA;
    private ImageView imageQC;
    private ImageView imageQ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (sla.getClosingRate() != null) {
            rootView = inflater.inflate(R.layout.fragment_sla_p5, container, false);

            txtQATarget = (TextView) rootView.findViewById(R.id.txtQATarget);
            txtQAPct = (TextView) rootView.findViewById(R.id.txtQAPct);
            txtQCTarget = (TextView) rootView.findViewById(R.id.txtQCTarget);
            txtQCPct = (TextView) rootView.findViewById(R.id.txtQCPct);
            txtQTotalTarget = (TextView) rootView.findViewById(R.id.txtQTotalTarget);
            txtQTotalPct = (TextView) rootView.findViewById(R.id.txtQTotalPct);

            imageQA = (ImageView) rootView.findViewById(R.id.imageQA);
            imageQC = (ImageView) rootView.findViewById(R.id.imageQC);
            imageQ = (ImageView) rootView.findViewById(R.id.imageQ);

            refresh();
        } else {
            Toast.makeText(getActivity().getBaseContext(), "Server Connection Error: Data cannot be retrieved. Please try again.", Toast.LENGTH_LONG).show();
        }

/*
        content_frame_nav = (LinearLayout) ((FragmentActivity) rootView.getContext()).findViewById(R.id.content_frame_nav);
        content_frame_nav.setVisibility(View.GONE);
*/


        return rootView;
    }

    public void setSLA(ServiceLevelAgreement sla) {
        this.sla = sla;
    }

    public void refresh() {
        Quality quality = sla.getQuality();
        if (quality != null) {
            txtQATarget.setText("TARGET: " + String.valueOf(quality.getQaTarget()) + "%");
            txtQAPct.setText(String.valueOf(quality.getQaPct()) + "%");
            if (quality.getQaTarget() > quality.getQaPct() || quality.getQaPct() == 0) {
                txtQAPct.setBackgroundResource(R.drawable.base_rate_negative);
                imageQA.setImageResource(R.drawable.ic_arrow_indicator_negative);
            } else {
                txtQAPct.setBackgroundResource(R.drawable.base_rate_positive);
                imageQA.setImageResource(R.drawable.ic_arrow_indicator_positive);
            }

            txtQCTarget.setText("TARGET: " + String.valueOf(quality.getQcTarget()) + "%");
            txtQCPct.setText(String.valueOf(quality.getQcPct()) + "%");
            if (quality.getQcTarget() > quality.getQcPct() || quality.getQcPct() == 0) {
                txtQCPct.setBackgroundResource(R.drawable.base_rate_negative);
                imageQC.setImageResource(R.drawable.ic_arrow_indicator_negative);
            } else {
                txtQCPct.setBackgroundResource(R.drawable.base_rate_positive);
                imageQC.setImageResource(R.drawable.ic_arrow_indicator_positive);
            }

            txtQTotalTarget.setText("TARGET: " + String.valueOf(quality.getQaqcTarget()) + "%");
            txtQTotalPct.setText(String.valueOf(quality.getQaqcPct()) + "%");
            if (quality.getQaqcTarget() > quality.getQaqcPct() || quality.getQaqcPct() == 0) {
                txtQTotalPct.setBackgroundResource(R.drawable.base_rate_negative);
                imageQ.setImageResource(R.drawable.ic_arrow_indicator_negative);
            } else {
                txtQTotalPct.setBackgroundResource(R.drawable.base_rate_positive);
                imageQ.setImageResource(R.drawable.ic_arrow_indicator_positive);
            }
        }


    }

}
