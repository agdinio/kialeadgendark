package com.almajid.kialeadgen;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.helpers.Common;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;

/**
 * Created by relly on 9/7/15.
 */
public class BaseActionBar {

    private Context context;
    private View actionBarLayout;
    private ActionBar supportActionBar;
    private DrawerLayout mDrawerLayout;
    private ImageButton btnDrawer;
    private ImageButton btnFilter;
    private LayerDrawable icon;
    private TextView txtActionBarTitle;
    private LeadSharedPreferences prefs;

    private Dialog dialog;

    private CheckBox chkVoice;
    private CheckBox chkEmail;
    private CheckBox chkLiveChat;
    private CheckBox chkSocialMedia;
    private CheckBox chkAll;
    private CheckBox chkIndividual;
    private CheckBox chkLuxury;
    private CheckBox chkWalkin;

    public BaseActionBar(Context context, DrawerLayout mDrawerLayout) {
        this.context = context;
        this.mDrawerLayout = mDrawerLayout;
        prefs = new LeadSharedPreferences(context);

        actionBarLayout = ((AppCompatActivity)context).getLayoutInflater().inflate(R.layout.base_actionbar, null);
        supportActionBar = ((AppCompatActivity)context).getSupportActionBar();
        supportActionBar.setDisplayShowHomeEnabled(false);
        supportActionBar.setDisplayShowTitleEnabled(false);
        supportActionBar.setDisplayShowCustomEnabled(true);
        supportActionBar.setCustomView(actionBarLayout);


        Toolbar parent = (Toolbar) supportActionBar.getCustomView().getParent();
        parent.setContentInsetsAbsolute(0, 0);

        btnDrawer = (ImageButton) actionBarLayout.findViewById(R.id.btnDrawer);
        btnDrawer.setOnClickListener(btnDrawerClickListener);

        btnFilter = (ImageButton) actionBarLayout.findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(btnFilterClickListener);

    }

    private View.OnClickListener btnDrawerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Common.hideKeyboard(context);
            toggleDrawer();
        }
    };

    private View.OnClickListener btnFilterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            mDrawerLayout.closeDrawer(Gravity.LEFT);

//            PopupMenu popup = new PopupMenu(context, actionBarLayout);
//            MenuInflater inflater = popup.getMenuInflater();
//            inflater.inflate(R.menu.filter, popup.getMenu());
//            popup.show();


/*
            Fragment frag;
            FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

            frag = fragmentManager.findFragmentByTag(Global.FRAG_FILTER);
            if (frag == null) {
                frag = new FragmentFilter();
            } else {
                fragmentManager.beginTransaction().remove(frag).commit();
                frag = new FragmentFilter();
            }
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.content_frame, frag, Global.FRAG_FILTER)
                    .addToBackStack(Global.FRAG_FILTER)
                    .commit();
*/


            dialog = new Dialog(context);
            dialog.getWindow();
            dialog.setCancelable(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_filter);

            Window w = dialog.getWindow();
            w.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            w.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


            chkVoice = (CheckBox) w.findViewById(R.id.chkVoice);
            chkEmail = (CheckBox) w.findViewById(R.id.chkEmail);
            chkLiveChat = (CheckBox) w.findViewById(R.id.chkLiveChat);
            chkSocialMedia = (CheckBox) w.findViewById(R.id.chkSocialMedia);
            chkAll = (CheckBox) w.findViewById(R.id.chkAllChannels);
            chkIndividual = (CheckBox) w.findViewById(R.id.chkIndividual);
            chkLuxury = (CheckBox) w.findViewById(R.id.chkLuxury);
            chkWalkin = (CheckBox) w.findViewById(R.id.chkWalkin);

            Filter filter = prefs.getFilter();
            if (filter != null) {
                chkVoice.setChecked(filter.isVoice());
                chkEmail.setChecked(filter.isEmail());
                chkLiveChat.setChecked(filter.isLiveChat());
                chkSocialMedia.setChecked(filter.isSocialMedia());
                chkAll.setChecked(filter.isAll());
                chkIndividual.setChecked(filter.isIndividual());
                chkLuxury.setChecked(filter.isLuxury());
                chkWalkin.setChecked(filter.isWalkin());
            }


            Button btnCancel = (Button) w.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            Button btnApply = (Button) w.findViewById(R.id.btnApply);
            btnApply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    prefs = new LeadSharedPreferences(context);
                    Filter filter = prefs.getFilter();
                    if (filter == null) {
                        filter = new Filter();
                        filter.setVoice(chkVoice.isChecked());
                        filter.setEmail(chkEmail.isChecked());
                        filter.setLiveChat(chkLiveChat.isChecked());
                        filter.setSocialMedia(chkSocialMedia.isChecked());
                        filter.setAll(chkAll.isChecked());
                        filter.setIndividual(chkIndividual.isChecked());
                        filter.setLuxury(chkLuxury.isChecked());
                        filter.setWalkin(chkWalkin.isChecked());
                        filter.setMtd(true);
                    } else {
                        filter.setVoice(chkVoice.isChecked());
                        filter.setEmail(chkEmail.isChecked());
                        filter.setLiveChat(chkLiveChat.isChecked());
                        filter.setSocialMedia(chkSocialMedia.isChecked());
                        filter.setAll(chkAll.isChecked());
                        filter.setIndividual(chkIndividual.isChecked());
                        filter.setLuxury(chkLuxury.isChecked());
                        filter.setWalkin(chkWalkin.isChecked());
                    }

                    String jsonString = prefs.toJson(filter);
                    prefs.editMode();
                    prefs.putString("FILTER", jsonString);
                    prefs.commitMode();

                    dialog.dismiss();
                }
            });

            dialog.show();

            Button btnDate = (Button) w.findViewById(R.id.btnDate);
            btnDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                    FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                    Fragment frag = fragmentManager.findFragmentByTag(Global.FRAG_FILTER_DATE);
                    if (frag == null) {
                        frag = new FragmentFilterDate();
                    } else {
                        fragmentManager.beginTransaction().remove(frag).commit();
                        frag = new FragmentFilterDate();
                    }
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.content_frame, frag, Global.FRAG_FILTER_DATE)
                            .addToBackStack(Global.FRAG_FILTER_DATE)
                            .commit();
                }
            });
        }
    };

    private void toggleDrawer() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
    }


}
