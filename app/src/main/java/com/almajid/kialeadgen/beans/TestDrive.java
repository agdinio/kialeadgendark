package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 11/22/15.
 */
public class TestDrive {

    private DailyTestDrive dailyTestDrive;
    private ContactType contactType;

    public DailyTestDrive getDailyTestDrive() {
        return dailyTestDrive;
    }

    public void setDailyTestDrive(DailyTestDrive dailyTestDrive) {
        this.dailyTestDrive = dailyTestDrive;
    }

    public ContactType getContactType() {
        return contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }
}
