package com.almajid.kialeadgen.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.almajid.kialeadgen.R;

import java.util.List;

/**
 * Created by relly on 11/21/15.
 */
public class DropdownYear extends ArrayAdapter<String> {

    private Context context;
    private int resource;
    private List<String> objects;

    public DropdownYear(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(resource, null);

            TextView txtEmirate = (TextView) v.findViewById(R.id.txtYear);
        }

        return v;
    }
}
