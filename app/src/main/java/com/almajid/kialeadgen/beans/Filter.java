package com.almajid.kialeadgen.beans;

import java.io.Serializable;

/**
 * Created by relly on 9/7/15.
 */
public class Filter implements Serializable {

    private boolean voice;
    private boolean email;
    private boolean liveChat;
    private boolean socialMedia;
    private boolean all;

    private boolean individual;
    private boolean luxury;
    private boolean walkin;
    private String dateRange;
    private String monthly;
    private boolean mtd;
    private String filterType;

    public boolean isVoice() {
        return voice;
    }

    public void setVoice(boolean voice) {
        this.voice = voice;
    }

    public boolean isEmail() {
        return email;
    }

    public void setEmail(boolean email) {
        this.email = email;
    }

    public boolean isLiveChat() {
        return liveChat;
    }

    public void setLiveChat(boolean liveChat) {
        this.liveChat = liveChat;
    }

    public boolean isSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(boolean socialMedia) {
        this.socialMedia = socialMedia;
    }

    public boolean isAll() {
        return all;
    }

    public void setAll(boolean all) {
        this.all = all;
    }

    public boolean isIndividual() {
        return individual;
    }

    public void setIndividual(boolean individual) {
        this.individual = individual;
    }

    public boolean isLuxury() {
        return luxury;
    }

    public void setLuxury(boolean luxury) {
        this.luxury = luxury;
    }

    public boolean isWalkin() {
        return walkin;
    }

    public void setWalkin(boolean walkin) {
        this.walkin = walkin;
    }

    public String getDateRange() {
        return dateRange;
    }

    public void setDateRange(String dateRange) {
        this.dateRange = dateRange;
    }

    public String getMonthly() {
        return monthly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public boolean isMtd() {
        return mtd;
    }

    public void setMtd(boolean mtd) {
        this.mtd = mtd;
    }

    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }
}
