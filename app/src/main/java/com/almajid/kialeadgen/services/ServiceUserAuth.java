package com.almajid.kialeadgen.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.almajid.kialeadgen.MainActivity;
import com.almajid.kialeadgen.beans.User;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by relly on 11/24/15.
 */
public class ServiceUserAuth extends Thread {

    private LeadSharedPreferences prefs;
    private Context context;
    private ProgressDialog dialog;
    private String username;
    private String password;
    private boolean isChecked;
    private Object[] params;


    public ServiceUserAuth(Context context, Object[] params) {
        this.context = context;
        this.params = params;

        prefs = new LeadSharedPreferences(context);
    }

    @Override
    public void run() {

        try {
            if (params != null && params.length > 0) {
                if (params[0] instanceof ProgressDialog) {
                    dialog = (ProgressDialog) params[0];
                }
                if (params[1] instanceof String) {
                    username = (String) params[1];
                }
                if (params[2] instanceof String) {
                    password = (String) params[2];
                }
                if (params[3] instanceof Boolean) {
                    isChecked = (boolean) params[3];
                }
            }

//            SoapObject request = new SoapObject(Global.NAMESPACE, Global.METHOD_NAME_AUTHENTICATE);
//            request.addProperty("username", username);
//            request.addProperty("password", password);
//
//            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//            envelope.dotNet = true;
//            envelope.setOutputSoapObject(request);
//
//            HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
//            try {
//                androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Global.SOAP_ACTION_AUTHENTICATE), envelope);
//
//                SoapObject result = (SoapObject) envelope.getResponse();
//                if (result != null && result.getPropertyCount() > 0) {
//
//                    User user = new User();
//                    user.setIdent(Integer.parseInt(result.getProperty("Ident").toString()));
//                    user.setObjId(result.getProperty("ObjId").toString());
//                    user.setUsername(result.getProperty("Username").toString());
//                    user.setPassword(result.getProperty("Password").toString());
//                    user.setName(result.getProperty("Name").toString());
//                    user.setType(result.getProperty("Type").toString());
//
//                    String jsonString = prefs.toJson(user);
//                    prefs.editMode();
//                    prefs.putString(Global.USER_INFO, jsonString);
//                    prefs.putBoolean(Global.REMEMBER_ME, isChecked);
//                    prefs.commitMode();
//
//                    Intent intent = new Intent(context, MainActivity.class);
//                    context.startActivity(intent);
//
//
//                } else {
//                    new ThreadHandler(context).sendEmptyMessage(0);
//                }
//
//            } catch (Exception e) {
//                new ThreadHandler(context).sendEmptyMessage(1);
//            }

            if (username.equalsIgnoreCase("1000") && password.equals("admin")) {
                    User user = new User();
                    user.setIdent(1000);
                    user.setObjId("MK3cIAwK");
                    user.setUsername("1000");
                    user.setPassword("admin");
                    user.setName("Admin Demo");
                    user.setType("Administrator");

                    String jsonString = prefs.toJson(user);
                    prefs.editMode();
                    prefs.putString(Global.USER_INFO, jsonString);
                    prefs.putBoolean(Global.REMEMBER_ME, isChecked);
                    prefs.commitMode();

                    Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);
            } else {
                    new ThreadHandler(context).sendEmptyMessage(0);
            }

        } catch (Exception ex) {
            Log.e("LOGCAT", (ex.getMessage() == null) ? ex.getMessage() : ex.toString());
            new ThreadHandler(context).sendEmptyMessage(1);
        } finally {
            dialog.dismiss();
        }


    }

    private class ThreadHandler extends Handler {

        private Context context;

        public ThreadHandler(Context context) {
            super(Looper.getMainLooper());
            this.context = context;
        }


        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0:
                    Toast.makeText(context, "Invalid username and/or password.", Toast.LENGTH_LONG).show();
                    break;
                case 1:
                    Toast.makeText(context, "Connection to server failed.", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }
}
