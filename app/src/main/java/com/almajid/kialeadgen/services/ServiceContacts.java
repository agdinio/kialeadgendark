package com.almajid.kialeadgen.services;

import android.content.Context;
import android.os.AsyncTask;

import com.almajid.kialeadgen.beans.Contacts;
import com.almajid.kialeadgen.beans.DailyContacts;
import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.interfaces.ContactsImpl;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by relly on 11/21/15.
 */
public class ServiceContacts extends AsyncTask<Void, Void, Contacts> {


    private Context context;
    private ContactsImpl callback;
    private LeadSharedPreferences prefs;

    public ServiceContacts(Context context, ContactsImpl callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected Contacts doInBackground(Void... params) {

        prefs = new LeadSharedPreferences(context);
        Filter filter = prefs.getFilter();
        JSONObject json = new JSONObject();
        try {
            json.put("CodeId", 2);
            json.put("All", filter.isAll());
            json.put("Voice", filter.isVoice());
            json.put("Chat", filter.isLiveChat());
            json.put("Email", filter.isEmail());
            json.put("SocialMedia", filter.isSocialMedia());
            json.put("Walkin", filter.isWalkin());
            json.put("Individual", filter.isIndividual());
            json.put("Luxury", filter.isLuxury());
            json.put("DateRange", filter.getDateRange());
            json.put("Monthly", filter.getMonthly());
            json.put("MTD", filter.isMtd());
        } catch (JSONException e) {
            return null;
        }


        Contacts contacts = new Contacts();

//        SoapObject request = new SoapObject(Global.NAMESPACE, Global.METHOD_NAME_GETCONTACTS);
//        request.addProperty("filters", json.toString());
//
//        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        envelope.dotNet = true;
//        envelope.setOutputSoapObject(request);
//
//        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
//        try {
//            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Global.SOAP_ACTION_GETCONTACTS), envelope);
//
//            SoapObject result = (SoapObject) envelope.getResponse();
//            if (result != null && result.getPropertyCount() > 0) {
//                SoapObject dcsoap = (SoapObject) result.getProperty("DailyContacts");
//                if (dcsoap != null) {
//                    DailyContacts dc = new DailyContacts();
//                    dc.setToday(Integer.parseInt(dcsoap.getProperty("Today").toString()));
//                    dc.setAverage(Integer.parseInt(dcsoap.getProperty("Average").toString()));
//                    dc.setDifference(Integer.parseInt(dcsoap.getProperty("Difference").toString()));
//                    dc.setTotal(Integer.parseInt(dcsoap.getProperty("Total").toString()));
//                    dc.setGraph(dcsoap.getProperty("Graph").toString());
//                    contacts.setDailyContacts(dc);
//                } else {
//                    contacts.setDailyContacts(new DailyContacts());
//                }
//            }
//
//        } catch (Exception e) {
//            contacts.setDailyContacts(new DailyContacts());
//            e.printStackTrace();
//        }

        DailyContacts dc = new DailyContacts();
        dc.setToday(3);
        dc.setAverage(14);
        dc.setDifference(11);
        dc.setTotal(217);
        dc.setGraph("11-01-2015=10=DATERANGE,11-02-2015=14=DATERANGE,11-03-2015=13=DATERANGE,11-04-2015=16=DATERANGE,11-05-2015=13=DATERANGE,11-07-2015=28=DATERANGE,11-08-2015=16=DATERANGE,11-09-2015=17=DATERANGE,11-10-2015=16=DATERANGE,11-11-2015=24=DATERANGE,11-12-2015=12=DATERANGE,11-14-2015=17=DATERANGE,11-15-2015=21=DATERANGE,");
        contacts.setDailyContacts(dc);

        return contacts;
    }

    @Override
    protected void onPostExecute(Contacts contacts) {
        callback.getResult(contacts);
    }
}
