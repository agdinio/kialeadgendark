package com.almajid.kialeadgen;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.almajid.kialeadgen.beans.ServiceLevelAgreement;
import com.almajid.kialeadgen.interfaces.ServiceLevelAgreementImpl;
import com.almajid.kialeadgen.services.ServiceSLA;
import com.androidpagecontrol.PageControl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentSLA extends Fragment {


    private View rootView;
    private ViewPager mPager;
    private PagerAdapter mPageAdapter;
    private LinearLayout content_frame_nav;
    private List<Class> fragments;
    private FragmentSLAp1 p1;
    private FragmentSLAp2 p2;
    private FragmentSLAp3 p3;
    private FragmentSLAp4 p4;
    private FragmentSLAp5 p5;
    private FragmentSLAp6 p6;
    private ProgressBar progress;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
         View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_sla));

        rootView = inflater.inflate(R.layout.fragment_sla, container, false);
        progress = (ProgressBar) rootView.findViewById(R.id.progress);
        progress.setVisibility(View.VISIBLE);

        new ServiceSLA(rootView.getContext(),
                new ServiceLevelAgreementImpl() {
            @Override
            public void getResult(ServiceLevelAgreement sla) {
                initFragments(sla);
                progress.setVisibility(View.INVISIBLE);
            }
        }).execute();




        getActivity().invalidateOptionsMenu();

        return rootView;
    }


    private void initFragments(ServiceLevelAgreement sla) {
        fragments = new ArrayList<>();
        fragments.add(FragmentSLAp1.class);
        fragments.add(FragmentSLAp2.class);
        fragments.add(FragmentSLAp3.class);
        fragments.add(FragmentSLAp4.class);
        fragments.add(FragmentSLAp5.class);
        fragments.add(FragmentSLAp6.class);

        mPageAdapter = new PageAdapter(getActivity().getSupportFragmentManager(), sla);
        mPager = (ViewPager) rootView.findViewById(R.id.pageContainer);
        mPager.setAdapter(mPageAdapter);
        mPager.setOffscreenPageLimit(0);
        //mPager.setCurrentItem(0);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position)   {
                    case 0:
                        if (p1 != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    p1.refresh();
                                }
                            }, 500);
                        }
                        break;
                    case 1:
                        if (p2 != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    p2.updateLabels();
                                    p2.refreshChart();
                                    p2.updateClosingRateChart();
                                    p2.updateVOCChart();
                                }
                            }, 500);
                        }
                        break;
                    case 2:
                        if (p3 != null) {
                            p3.refresh();
                        }
                        break;
                    case 3:
                        if (p4 != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    p4.refreshChart();
                                    p4.updateAttendanceRateChart();
                                    p4.updateQualificationRateChart();
                                }
                            }, 500);
                        }
                        break;
                    case 4:
                        if (p5 != null) {
                            p5.refresh();
                        }
                        break;
                    case 5:
                        if (p6 != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                p6.updateLabels();
                                }
                            }, 500);
                        }
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        PageControl pageControl = (PageControl) rootView.findViewById(R.id.page_control);
        pageControl.setViewPager(mPager);
        pageControl.setPosition(0);

    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        private ServiceLevelAgreement sla;

        public PageAdapter(FragmentManager fm, ServiceLevelAgreement sla) {
            super(fm);
            this.sla = sla;
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    p1 = new FragmentSLAp1();
                    p1.setSLA(sla);
                    return p1;
                case 1:
                    p2 = new FragmentSLAp2();
                    p2.setSLA(sla);
                    return p2;
                case 2:
                    p3 = new FragmentSLAp3();
                    p3.setSLA(sla);
                    return  p3;
                case 3:
                    p4 = new FragmentSLAp4();
                    p4.setSLA(sla);
                    return p4;
                case 4:
                    p5 = new FragmentSLAp5();
                    p5.setSLA(sla);
                    return p5;
                case 5:
                    p6 = new FragmentSLAp6();
                    p6.setSLA(sla);
                    return p6;
                default:
                    return new FragmentSLAp1();
            }

            //return Fragment.instantiate(rootView.getContext(), fragments.get(position).getName());
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

//        @Override
//        public boolean isViewFromObject(View view, Object object) {
//            if (object != null) {
//                return ((Fragment)object).getView() == view;
//            } else {
//                return false;
//            }
//        }

        @Override
        public int getCount() {
            return fragments.size();
        }

    }


/*
    private class CustomPageAdapter extends PagerAdapter {

        private Context context;

        public CustomPageAdapter(Context context) {
            this.context = context;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            CustomPagerEnum page = CustomPagerEnum.values()[position];
            LayoutInflater inflater = LayoutInflater.from(context);
            ViewGroup layout = (ViewGroup) inflater.inflate(page.layoutResId, container, false);
            container.addView(layout);

            return layout;

        }

        @Override
        public int getCount() {
            return CustomPagerEnum.values().length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    private enum  CustomPagerEnum {

        P1(R.layout.fragment_sla_p1),
        P2(R.layout.fragment_sla_p2),
        P3(R.layout.fragment_sla_p3),
        P4(R.layout.fragment_sla_p4),
        P5(R.layout.fragment_sla_p5);

        private int layoutResId;

        CustomPagerEnum(int layoutResId) {
            this.layoutResId = layoutResId;
        }


    }
*/

}
