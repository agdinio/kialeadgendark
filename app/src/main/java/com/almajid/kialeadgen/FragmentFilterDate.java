package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

/**
 * Created by relly on 9/8/15.
 */
public class FragmentFilterDate extends Fragment {

    private View rootView;
    private FragmentTabHost tabhost;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_filter_date));

        rootView = inflater.inflate(R.layout.fragment_filterdate, container, false);

        tabhost = (FragmentTabHost) rootView.findViewById(R.id.tabhost);
        tabhost.setup(rootView.getContext(), getChildFragmentManager(), R.id.realtabcontent);
        tabhost.addTab(tabhost.newTabSpec("0").setIndicator("Date Range"), FragmentFilterDateRange.class, null);
        tabhost.addTab(tabhost.newTabSpec("1").setIndicator("MTD"), FragmentFilterMTD.class, null);
        tabhost.addTab(tabhost.newTabSpec("2").setIndicator("Monthly"), FragmentFilterMonthly.class, null);
        tabhost.getTabWidget().setStripEnabled(false);
        tabhost.getTabWidget().setDividerDrawable(null);

        setTabTextColor();

        tabhost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                setTabTextColor();
            }
        });

        return rootView;
    }

    private void setTabTextColor() {
        for (int i=0; i<tabhost.getTabWidget().getChildCount(); i++) {
            View v = tabhost.getTabWidget().getChildAt(i);

            v.setBackgroundResource(R.drawable.date_filter_tab_default);
            TextView tv = (TextView) tabhost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(Color.argb(255, 255, 255, 255));
        }
        View v = tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab());
        v.setBackgroundResource(R.drawable.date_filter_tab_selected);
        TextView tv = (TextView) tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab()).findViewById(android.R.id.title);
        tv.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.accent));
    }


}
