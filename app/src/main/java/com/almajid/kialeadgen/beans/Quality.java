package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 11/18/15.
 */
public class Quality {

    private int qaTarget;
    private int qcTarget;
    private int qaqcTarget;
    private int qa;
    private int qc;
    private int qaqcTotal;
    private int qaPct;
    private int qcPct;
    private int qaqcPct;

    public int getQaTarget() {
        return qaTarget;
    }

    public void setQaTarget(int qaTarget) {
        this.qaTarget = qaTarget;
    }

    public int getQcTarget() {
        return qcTarget;
    }

    public void setQcTarget(int qcTarget) {
        this.qcTarget = qcTarget;
    }

    public int getQaqcTarget() {
        return qaqcTarget;
    }

    public void setQaqcTarget(int qaqcTarget) {
        this.qaqcTarget = qaqcTarget;
    }

    public int getQa() {
        return qa;
    }

    public void setQa(int qa) {
        this.qa = qa;
    }

    public int getQc() {
        return qc;
    }

    public void setQc(int qc) {
        this.qc = qc;
    }

    public int getQaqcTotal() {
        return qaqcTotal;
    }

    public void setQaqcTotal(int qaqcTotal) {
        this.qaqcTotal = qaqcTotal;
    }

    public int getQaPct() {
        return qaPct;
    }

    public void setQaPct(int qaPct) {
        this.qaPct = qaPct;
    }

    public int getQcPct() {
        return qcPct;
    }

    public void setQcPct(int qcPct) {
        this.qcPct = qcPct;
    }

    public int getQaqcPct() {
        return qaqcPct;
    }

    public void setQaqcPct(int qaqcPct) {
        this.qaqcPct = qaqcPct;
    }
}
