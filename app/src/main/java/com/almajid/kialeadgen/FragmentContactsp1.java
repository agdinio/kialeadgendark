package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.Contacts;
import com.almajid.kialeadgen.beans.DailyContacts;
import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.PercentFormatter;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by relly on 9/8/15.
 */
public class FragmentContactsp1 extends Fragment {

    private LeadSharedPreferences prefs;
    private View rootView;
    private LineChart contactsChart;
    private Contacts contacts;

    private TextView txtTotal;
    private TextView txtToday;
    private TextView txtAve;
    private TextView txtDiff;
    private ImageView imageArrow;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (contacts.getDailyContacts() != null) {
            rootView = inflater.inflate(R.layout.fragment_contacts_p1, container, false);
            contactsChart = (LineChart) rootView.findViewById(R.id.contactsChart);
            txtTotal = (TextView) rootView.findViewById(R.id.txtTotal);
            txtToday = (TextView) rootView.findViewById(R.id.txtToday);
            txtAve = (TextView) rootView.findViewById(R.id.txtAve);
            txtDiff = (TextView) rootView.findViewById(R.id.txtDiff);
            imageArrow = (ImageView) rootView.findViewById(R.id.imageArrow);

            updateLabels();
            updateChart();
        } else {
            Toast.makeText(getActivity().getBaseContext(), "Server Connection Error: Data cannot be retrieved. Please try again.", Toast.LENGTH_LONG).show();
        }

        return rootView;
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts;
    }

    public void updateLabels() {
        DailyContacts dc = contacts.getDailyContacts();
        if (dc != null) {
            txtTotal.setText(String.valueOf(dc.getTotal()));
            txtToday.setText(String.valueOf(dc.getToday()));
            txtAve.setText(String.valueOf(dc.getAverage()));
            txtDiff.setText(String.valueOf(dc.getDifference()));

            if (dc.getAverage() > dc.getToday()) {
                imageArrow.setImageResource(R.drawable.ic_arrow_indicator_negative);
            } else {
                imageArrow.setImageResource(R.drawable.ic_arrow_indicator_positive);
            }
        }
    }

    public void updateChart() {
        prefs = new LeadSharedPreferences(rootView.getContext());
        Filter filter = prefs.getFilter();
        if (filter == null) {
            return;
        }
        DailyContacts dc = contacts.getDailyContacts();

        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> values = new ArrayList<Entry>();


        String dca[] = TextUtils.isEmpty(dc.getGraph()) ? new String[]{} : dc.getGraph().split(",");

        if (filter.getFilterType().equalsIgnoreCase("MONTHLY")) {
            SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
            Calendar calFilterDate = Calendar.getInstance();
            Calendar calDateWithValue = Calendar.getInstance();
            Date filterdate = null;

            String curr_filters[] = filter.getMonthly().split(",");

            for (int i = 0; i < curr_filters.length; i++) {
                try {
                    filterdate = df.parse(curr_filters[i]);
                    calFilterDate.setTime(filterdate);

                    String str_month = Global.months[calFilterDate.get(Calendar.MONTH)];

                    xVals.add(str_month + " " + calFilterDate.get(Calendar.YEAR));
                    values.add(new Entry(0, i));

                    for (int j = 0; j < dca.length; j++) {


                        String ar[] = dca[j].split("=");

                        Date datewithvalue = df.parse(ar[0]);
                        calDateWithValue.setTime(datewithvalue);

                        if (calDateWithValue.get(Calendar.MONTH) == calFilterDate.get(Calendar.MONTH)
                                && calDateWithValue.get(Calendar.YEAR) == calFilterDate.get(Calendar.YEAR)) {
                            int e = Integer.parseInt(ar[1]);
                            values.set(i, new Entry(e, i));
                        }

                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

        } else if (filter.getFilterType().equalsIgnoreCase("DATERANGE")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM-dd-''yy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            String curr_filters[] = filter.getDateRange().split(",");

            try {
                Date sdate = dffrom.parse(curr_filters[0]);
                Date edate = dfto.parse(curr_filters[1]);
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new Entry(0, i));

                for (int j=0; j<dca.length; j++) {

                    String ar[] = dca[j].split("=");

                    if (ar[0].equalsIgnoreCase((dfto.format(targetday)))) {
                        //float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        int e = Integer.parseInt(ar[1]);
                        values.set(i, new Entry(e, i));
                    }
                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        } else if (filter.getFilterType().equalsIgnoreCase("MTD")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-01-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM-dd-''yy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            try {
                Date sdate = dffrom.parse(dffrom.format(new Date()));
                Date edate = dfto.parse(dfto.format(new Date()));
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new Entry(0, i));

                for (int j=0; j<dca.length; j++) {


                    String ar[] = dca[j].split("=");

                    if (ar[0].equalsIgnoreCase(dfto.format(targetday))) {
                        int e = Integer.parseInt(ar[1]);
                        values.set(i, new Entry(e, i));
                    }

                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        }


        contactsChart.setDrawGridBackground(false);
        contactsChart.setDescription("");
        //crChart.setHighlightEnabled(false);
        //crChart.setTouchEnabled(false);
        //crChart.setScaleEnabled(true);
        contactsChart.setPinchZoom(true);

        contactsChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
        contactsChart.resetTracking();
        contactsChart.getAxisRight().setDrawLabels(false);
        contactsChart.getAxisLeft().setDrawLabels(false);
        contactsChart.getLegend().setEnabled(false);




        XAxis xaxis = contactsChart.getXAxis();
        xaxis.setTextSize(8f);
        xaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        xaxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis lyaxis = contactsChart.getAxisLeft();
        lyaxis.setTextSize(8f);
        lyaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        //lyaxis.setAxisMaxValue(15f);

        LineDataSet d = new LineDataSet(values, "Daily Contacts");
        d.setValueTextSize(7f);
        d.setColor(Color.YELLOW);
        d.setDrawCircles(false);
        d.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        //d.setValueFormatter(new PercentFormatter());
        LineData data = new LineData(xVals, d);

        contactsChart.setData(data);
        contactsChart.invalidate();

    }
}
