package com.almajid.kialeadgen.drawer;

/**
 * Created by relly on 7/1/15.
 */
public interface NavDrawerItemImpl {

    int getId();
    String getLabel();
    String getName();
    int getType();
    boolean isEnabled();
    boolean updateActionBarTitle();

}
