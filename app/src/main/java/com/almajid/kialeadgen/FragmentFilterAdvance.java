package com.almajid.kialeadgen;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;

/**
 * Created by relly on 9/8/15.
 */
public class FragmentFilterAdvance extends Fragment {

    private CheckBox chkVoice;
    private CheckBox chkEmail;
    private CheckBox chkLiveChat;
    private CheckBox chkSocialMedia;
    private CheckBox chkAll;
    private CheckBox chkIndividual;
    private CheckBox chkLuxury;

    private View rootView;
    private LeadSharedPreferences prefs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_filter));

        rootView = inflater.inflate(R.layout.fragment_filteradvance, container, false);

        prefs = new LeadSharedPreferences(rootView.getContext());
        chkVoice = (CheckBox) rootView.findViewById(R.id.chkVoice);
        chkEmail = (CheckBox) rootView.findViewById(R.id.chkEmail);
        chkLiveChat = (CheckBox) rootView.findViewById(R.id.chkLiveChat);
        chkSocialMedia = (CheckBox) rootView.findViewById(R.id.chkSocialMedia);
        chkAll = (CheckBox) rootView.findViewById(R.id.chkAllChannels);
        chkIndividual = (CheckBox) rootView.findViewById(R.id.chkIndividual);
        chkLuxury = (CheckBox) rootView.findViewById(R.id.chkLuxury);

        Filter filter = prefs.getFilter();
        if (filter != null) {
            chkVoice.setChecked(filter.isVoice());
            chkEmail.setChecked(filter.isEmail());
            chkLiveChat.setChecked(filter.isLiveChat());
            chkSocialMedia.setChecked(filter.isSocialMedia());
            chkAll.setChecked(filter.isAll());
            chkIndividual.setChecked(filter.isIndividual());
            chkLuxury.setChecked(filter.isLuxury());
        }


/*
        Button btnApply = (Button) rootView.findViewById(R.id.btnApply);
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Filter filter = new Filter();
                filter.setVoice(chkVoice.isChecked());
                filter.setEmail(chkEmail.isChecked());
                filter.setLiveChat(chkLiveChat.isChecked());
                filter.setSocialMedia(chkSocialMedia.isChecked());
                filter.setAll(chkAll.isChecked());
                filter.setIndividual(chkIndividual.isChecked());
                filter.setLuxury(chkLuxury.isChecked());

                String jsonString = prefs.toJson(filter);
                prefs.editMode();
                prefs.putString("FILTER", jsonString);
                prefs.commitMode();

            }
        });
*/

        return rootView;
    }

}
