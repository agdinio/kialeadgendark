package com.almajid.kialeadgen;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.almajid.kialeadgen.abstracts.BaseFragment;
import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.beans.User;
import com.almajid.kialeadgen.drawer.NavDrawerAdapter;
import com.almajid.kialeadgen.drawer.NavDrawerItemImpl;
import com.almajid.kialeadgen.drawer.NavMenuItem;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;

/**
 * Created by relly on 9/7/15.
 */
public class MainActivity extends AppCompatActivity {

    private LeadSharedPreferences prefs;
    private Context context = this;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private BaseActionBar actionBar;
    private CharSequence mMenuTitle;
    private CharSequence mMenuName;
    private String[] mMenuTitles;
    private NavDrawerItemImpl[] menu;
    private BaseFragment selectedFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        prefs = new LeadSharedPreferences(context);

        User user = prefs.getUser();
        if (user == null) {
            Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(loginIntent);
        }

        menu = new NavDrawerItemImpl[]{
                NavMenuItem.create(101, "Welcome \n" + user.getName() + "!", 0, "welcome", "WELCOME"),
                NavMenuItem.create(102, "Service Level Agreement", 0, "sla", "SLA"),
                NavMenuItem.create(103, "Contacts", 0, "contacts", "CONTACTS"),
                NavMenuItem.create(104, "Test Drive", 0, "testdrive", "TEST DRIVE"),
                NavMenuItem.create(105, "Test Drive Leads", 0, "leads", "TEST DRIVE LEADS"),
                NavMenuItem.create(106, "Log Out", 0, "logout", "LOGOUT")
        };
        mDrawerList.setAdapter(new NavDrawerAdapter(this, 0, menu));
        mDrawerList.setAdapter(new NavDrawerAdapter(this, 0, menu));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                /////actionBar.toggleSearchButtonVisibility(0);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                /////actionBar.toggleSearchButtonVisibility(1);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        adjustDrawerWidth();
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        actionBar = new BaseActionBar(MainActivity.this, mDrawerLayout);

        if (savedInstanceState == null) {
            selectFragment(null, 0);
        }


    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            if (position != 0) {
                Filter filter = prefs.getFilter();

                if (filter == null) {
                    new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setTitle("Error")
                            .setMessage("Please set the Date Filter, then continue")
                            .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .create()
                            .show();
                    return;
                }

            }

            selectFragment(parent, position);
        }
    }

    private void selectFragment(AdapterView<?> parent, int position) {

        Fragment frag;
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (parent == null) {
            frag = new FragmentFilterDate();fragmentManager
                    .beginTransaction()
                    .replace(R.id.content_frame, frag, Global.FRAG_FILTER_DATE)
                    .addToBackStack(Global.FRAG_FILTER_DATE)
                    .commit();
            mMenuTitle = "REFINE";
            mMenuName = "refine";
        } else {
            NavMenuItem item = (NavMenuItem) parent.getAdapter().getItem(position);
            mMenuName = item.getName();
            mMenuTitle = item.getTitle();

            if (mMenuName.toString().equalsIgnoreCase("WELCOME")) {
                frag = fragmentManager.findFragmentByTag(Global.FRAG_FILTER_DATE);
                if (frag == null) {
                    frag = new FragmentFilterDate();
                } else {
                    fragmentManager.beginTransaction().remove(frag).commit();
                    frag = new FragmentFilterDate();
                }
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_frame, frag, Global.FRAG_FILTER_DATE)
                        .addToBackStack(Global.FRAG_FILTER_DATE)
                        .commit();

            } else if (mMenuName.toString().equalsIgnoreCase("SLA")) {
                frag = fragmentManager.findFragmentByTag(Global.FRAG_SLA);
                if (frag == null) {
                    frag = new FragmentSLA();
                } else {
                    fragmentManager.beginTransaction().remove(frag).commit();
                    frag = new FragmentSLA();
                }
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_frame, frag, Global.FRAG_SLA)
                        .commit();

            } else if (mMenuName.toString().equalsIgnoreCase("CONTACTS")) {
                frag = fragmentManager.findFragmentByTag(Global.FRAG_CONTACTS);
                if (frag == null) {
                    frag = new FragmentContacts();
                } else {
                    fragmentManager.beginTransaction().remove(frag).commit();
                    frag = new FragmentContacts();
                }
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_frame, frag, Global.FRAG_CONTACTS)
                        .addToBackStack(Global.FRAG_CONTACTS)
                        .commit();

            } else if (mMenuName.toString().equalsIgnoreCase("TESTDRIVE")) {
                frag = fragmentManager.findFragmentByTag(Global.FRAG_TESTDRIVE);
                if (frag == null) {
                    frag = new FragmentTestDrive();
                } else {
                    fragmentManager.beginTransaction().remove(frag).commit();
                    frag = new FragmentTestDrive();
                }
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_frame, frag, Global.FRAG_TESTDRIVE)
                        .addToBackStack(Global.FRAG_TESTDRIVE)
                        .commit();
            } else if (mMenuName.toString().equalsIgnoreCase("LEADS")) {
                frag = fragmentManager.findFragmentByTag(Global.FRAG_LEADS);
                if (frag == null) {
                    frag = new FragmentLeads();
                } else {
                    fragmentManager.beginTransaction().remove(frag).commit();
                    frag = new FragmentLeads();
                }
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_frame, frag, Global.FRAG_LEADS)
                        .addToBackStack(Global.FRAG_LEADS)
                        .commit();

            } else if (mMenuName.toString().equalsIgnoreCase("LOGOUT")) {
                confirmLogout();
            }

        }

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);

    }


    private void confirmLogout() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle("Confirm");
        dialog.setMessage("Are you sure you want to log out?");
        dialog.setPositiveButton("Log out", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handleCustomerInfo();
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.create().show();

    }

    private void handleCustomerInfo() {
        prefs.editMode();
        prefs.putString(Global.USER_INFO, "");
        prefs.putBoolean(Global.REMEMBER_ME, false);
        prefs.commitMode();

        Intent i = new Intent(context, LoginActivity.class);
        startActivity(i);
    }


    @SuppressLint("NewApi")
    private void adjustDrawerWidth() {

        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);

        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        try {

            DrawerLayout.LayoutParams layoutParams = (DrawerLayout.LayoutParams) mDrawerList.getLayoutParams();
            layoutParams.width = (size.x - 100);
            mDrawerList.setLayoutParams(layoutParams);

        } catch(Exception e) {
            e.printStackTrace();
        }

    }

}
