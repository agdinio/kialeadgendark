package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.almajid.kialeadgen.beans.AttendanceRate;
import com.almajid.kialeadgen.beans.ClosingRate;
import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.beans.QualificationRate;
import com.almajid.kialeadgen.beans.ServiceLevelAgreement;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentSLAp4 extends Fragment {


    private BarChart vocChart;
    private View rootView;
    private LinearLayout content_frame_nav;
    private LeadSharedPreferences prefs;
    private ServiceLevelAgreement sla;

    private LineChart arChart;
    private LineChart qrChart;
    private LineChart slChart;

    private TextView txtARToday;
    private TextView txtARAve;
    private TextView txtQRToday;
    private TextView txtQRAve;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_sla_p4, container, false);
        arChart = (LineChart) rootView.findViewById(R.id.arChart);
        qrChart = (LineChart) rootView.findViewById(R.id.qrChart);
        slChart = (LineChart) rootView.findViewById(R.id.slChart);

        txtARToday = (TextView) rootView.findViewById(R.id.txtARToday);
        txtARAve = (TextView) rootView.findViewById(R.id.txtARAve);
        txtQRToday = (TextView) rootView.findViewById(R.id.txtQRToday);
        txtQRAve = (TextView) rootView.findViewById(R.id.txtQRAve);

        prefs = new LeadSharedPreferences(rootView.getContext());

        //updateClosingRateChart();
        //updateVOCChart();

        return rootView;
    }

    public void refreshChart() {
        arChart.resetTracking();
        arChart.setData(null);
        arChart.invalidate();

        qrChart.resetTracking();
        qrChart.setData(null);
        qrChart.invalidate();

        slChart.resetTracking();
        slChart.setData(null);
        slChart.invalidate();

    }

    public void setSLA(ServiceLevelAgreement sla) {
        this.sla = sla;
    }

    public void updateAttendanceRateChart() {
        Filter filter = prefs.getFilter();
        if (filter == null) {
            return;
        }
        AttendanceRate attRate = sla.getAttendanceRate();
        if (TextUtils.isEmpty(attRate.getAttendedGraph())) {
            return;
        }

        txtARToday.setText( String.valueOf(attRate.getAttendedTodayCCPct()) + "%" );
        txtARAve.setText( String.valueOf(attRate.getAttendedCCPct()) + "%" );
        if (attRate.getAttendedTarget() > attRate.getAttendedCCPct())
            txtARAve.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.rate_negative));
        else
            txtARAve.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.rate_positive));


        Legend l = arChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));

        arChart.setDrawGridBackground(false);
        arChart.setDescription("");
        arChart.setPinchZoom(false);

        arChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
        arChart.resetTracking();
        arChart.getAxisRight().setDrawLabels(false);
        arChart.getAxisLeft().setDrawLabels(false);
        arChart.getLegend().setEnabled(false);

        String ara[] =  TextUtils.isEmpty(attRate.getAttendedGraph()) ? new String[]{} : attRate.getAttendedGraph().split(",");

        List<String> xVals = new ArrayList<>();
        List<Entry> values = new ArrayList<>();


        if (filter.getFilterType().equalsIgnoreCase("MONTHLY")) {
            SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
            Calendar calFilterDate = Calendar.getInstance();
            Calendar calDateWithValue = Calendar.getInstance();
            Date filterdate = null;

            String curr_filters[] = filter.getMonthly().split(",");

            for (int i = 0; i < curr_filters.length; i++) {
                try {
                    filterdate = df.parse(curr_filters[i]);
                    calFilterDate.setTime(filterdate);

                    String str_month = Global.months[calFilterDate.get(Calendar.MONTH)];

                    xVals.add(str_month + " " + calFilterDate.get(Calendar.YEAR));
                    values.add(new BarEntry(Float.parseFloat("0"), i));

                    for (int j = 0; j < ara.length; j++) {


                        String ar[] = ara[j].split("=");

                        Date datewithvalue = df.parse(ar[0]);
                        calDateWithValue.setTime(datewithvalue);

                        if (calDateWithValue.get(Calendar.MONTH) == calFilterDate.get(Calendar.MONTH)
                                && calDateWithValue.get(Calendar.YEAR) == calFilterDate.get(Calendar.YEAR)) {
                            float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                            values.set(i, new BarEntry(e, i));
                        }

                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

        } else if (filter.getFilterType().equalsIgnoreCase("DATERANGE")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM/dd/yyyy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            String curr_filters[] = filter.getDateRange().split(",");

            try {
                Date sdate = dffrom.parse(curr_filters[0]);
                Date edate = dfto.parse(curr_filters[1]);
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new Entry( Float.parseFloat("0"), i));

                for (int j=0; j<ara.length; j++) {

                    String ar[] = ara[j].split("=");

                    if (ar[0].equalsIgnoreCase((dfto.format(targetday)))) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new Entry(e, i));
                    }
                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        } else if (filter.getFilterType().equalsIgnoreCase("MTD")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-01-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM/dd/yyyy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            try {
                Date sdate = dffrom.parse(dffrom.format(new Date()));
                Date edate = dfto.parse(dfto.format(new Date()));
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new Entry( Float.parseFloat("0"), i));

                for (int j=0; j<ara.length; j++) {


                    String ar[] = ara[j].split("=");

                    if (ar[0].equalsIgnoreCase(dfto.format(targetday))) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new Entry(e, i));
                    }

                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        }


        XAxis xaxis = arChart.getXAxis();
        xaxis.setTextSize(8f);
        xaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        xaxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis lyaxis = arChart.getAxisLeft();
        lyaxis.setTextSize(8f);
        lyaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        //lyaxis.setAxisMaxValue(15f);

        LineDataSet d = new LineDataSet(values, "Closing Rate");
        d.setValueTextSize(8f);
        d.setColor(Color.YELLOW);
        d.setDrawCircles(false);
        d.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        d.setValueFormatter(new PercentFormatter());
        LineData data = new LineData(xVals, d);

        arChart.setData(data);
        arChart.invalidate();

    }

    public void updateQualificationRateChart() {
        Filter filter = prefs.getFilter();
        if (filter == null) {
            return;
        }
        QualificationRate qualRate = sla.getQualificationRate();

        txtQRToday.setText(String.valueOf(qualRate.getQualifiedTodayPct()) + "%");
        txtQRAve.setText(String.valueOf(qualRate.getQualifiedPct()) + "%");
        if (qualRate.getQualificationTarget() > qualRate.getQualifiedPct())
            txtQRAve.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.rate_negative));
        else
            txtQRAve.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.rate_positive));


        Legend l = qrChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));

        qrChart.setDrawGridBackground(false);
        qrChart.setDescription("");
        qrChart.setPinchZoom(false);

        qrChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
        qrChart.resetTracking();
        qrChart.getAxisRight().setDrawLabels(false);
        qrChart.getAxisLeft().setDrawLabels(false);
        qrChart.getLegend().setEnabled(false);

        String qra[] = TextUtils.isEmpty(qualRate.getQualifiedGraph()) ? new String[]{} : qualRate.getQualifiedGraph().split(",");

        List<String> xVals = new ArrayList<>();
        List<Entry> values = new ArrayList<>();


        if (filter.getFilterType().equalsIgnoreCase("MONTHLY")) {
            SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
            Calendar calFilterDate = Calendar.getInstance();
            Calendar calDateWithValue = Calendar.getInstance();
            Date filterdate = null;

            String curr_filters[] = filter.getMonthly().split(",");

            for (int i = 0; i < curr_filters.length; i++) {
                try {
                    filterdate = df.parse(curr_filters[i]);
                    calFilterDate.setTime(filterdate);

                    String str_month = Global.months[calFilterDate.get(Calendar.MONTH)];

                    xVals.add(str_month + " " + calFilterDate.get(Calendar.YEAR));
                    values.add(new BarEntry(Float.parseFloat("0"), i));

                    for (int j = 0; j < qra.length; j++) {


                        String ar[] = qra[j].split("=");

                        Date datewithvalue = df.parse(ar[0]);
                        calDateWithValue.setTime(datewithvalue);

                        if (calDateWithValue.get(Calendar.MONTH) == calFilterDate.get(Calendar.MONTH)
                                && calDateWithValue.get(Calendar.YEAR) == calFilterDate.get(Calendar.YEAR)) {
                            float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                            values.set(i, new BarEntry(e, i));
                        }

                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

        } else if (filter.getFilterType().equalsIgnoreCase("DATERANGE")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM/dd/yyyy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            String curr_filters[] = filter.getDateRange().split(",");

            try {
                Date sdate = dffrom.parse(curr_filters[0]);
                Date edate = dfto.parse(curr_filters[1]);
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new Entry( Float.parseFloat("0"), i));

                for (int j=0; j<qra.length; j++) {

                    String ar[] = qra[j].split("=");

                    if (ar[0].equalsIgnoreCase((dfto.format(targetday)))) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new Entry(e, i));
                    }
                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        } else if (filter.getFilterType().equalsIgnoreCase("MTD")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-01-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM/dd/yyyy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            try {
                Date sdate = dffrom.parse(dffrom.format(new Date()));
                Date edate = dfto.parse(dfto.format(new Date()));
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new Entry( Float.parseFloat("0"), i));

                for (int j=0; j<qra.length; j++) {


                    String ar[] = qra[j].split("=");

                    if (ar[0].equalsIgnoreCase(dfto.format(targetday))) {
                        float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        values.set(i, new Entry(e, i));
                    }

                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        }


        XAxis xaxis = qrChart.getXAxis();
        xaxis.setTextSize(8f);
        xaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        xaxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis lyaxis = qrChart.getAxisLeft();
        lyaxis.setTextSize(8f);
        lyaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        //lyaxis.setAxisMaxValue(15f);

        LineDataSet d = new LineDataSet(values, "Closing Rate");
        d.setValueTextSize(8f);
        d.setColor(Color.YELLOW);
        d.setDrawCircles(false);
        d.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        d.setValueFormatter(new PercentFormatter());
        LineData data = new LineData(xVals, d);

        qrChart.setData(data);
        qrChart.invalidate();

    }

    private void updateVOCChart() {

        int xx = 10;
        int yy = 100;

        vocChart = (BarChart) rootView.findViewById(R.id.vocChart);

        vocChart.setDescription("");
        vocChart.setMaxVisibleValueCount(60);
        vocChart.setPinchZoom(false);
        vocChart.setDrawBarShadow(false);
        vocChart.setDrawGridBackground(false);

        XAxis xAxis = vocChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setSpaceBetweenLabels(0);
        xAxis.setDrawGridLines(false);

        vocChart.getAxisLeft().setDrawGridLines(false);
        vocChart.animateY(2500);
        vocChart.getLegend().setEnabled(false);
        vocChart.getAxisRight().setDrawLabels(false);

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

        for (int i = 0; i < xx + 1; i++) {
            float mult = (yy + 1);
            float val1 = (float) (Math.random() * mult) + mult / 3;
            yVals1.add(new BarEntry((int) val1, i));
        }

        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < xx + 1; i++) {
            xVals.add((int) yVals1.get(i).getVal() + "");
        }

        BarDataSet set1 = new BarDataSet(yVals1, "Data Set");
        set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
        set1.setDrawValues(false);

        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(set1);

        BarData data = new BarData(xVals, dataSets);

        vocChart.setData(data);
        vocChart.invalidate();

    }

}
