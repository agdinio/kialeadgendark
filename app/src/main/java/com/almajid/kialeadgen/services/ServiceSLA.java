package com.almajid.kialeadgen.services;

import android.content.Context;
import android.net.http.HttpResponseCache;
import android.os.AsyncTask;

import com.almajid.kialeadgen.beans.AttendanceRate;
import com.almajid.kialeadgen.beans.ClosingRate;
import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.beans.QualificationRate;
import com.almajid.kialeadgen.beans.Quality;
import com.almajid.kialeadgen.beans.ServiceLevelAgreement;
import com.almajid.kialeadgen.beans.VoiceOfCustomer;
import com.almajid.kialeadgen.interfaces.ServiceLevelAgreementImpl;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by relly on 10/6/15.
 */
public class ServiceSLA extends AsyncTask<Void, Void, ServiceLevelAgreement> {

    ServiceLevelAgreementImpl callback;
    private Context context;
    private LeadSharedPreferences prefs;


    public ServiceSLA(Context context, ServiceLevelAgreementImpl callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected ServiceLevelAgreement doInBackground(Void... params) {

        prefs = new LeadSharedPreferences(context);
        Filter filter = prefs.getFilter();
        JSONObject json = new JSONObject();
        try {
            json.put("CodeId", 2);
            json.put("StatusCode", "WON");
            json.put("All",filter.isAll());
            json.put("Voice", filter.isVoice());
            json.put("Chat", filter.isLiveChat());
            json.put("Email", filter.isEmail());
            json.put("SocialMedia", filter.isSocialMedia());
            json.put("Walkin", filter.isWalkin());
            json.put("Individual", filter.isIndividual());
            json.put("Luxury", filter.isLuxury());
            json.put("DateRange", filter.getDateRange());
            json.put("Monthly", filter.getMonthly());
            json.put("MTD", filter.isMtd());
        } catch (JSONException e) {
            return null;
        }


        ServiceLevelAgreement sla = new ServiceLevelAgreement();

//        SoapObject request = new SoapObject(Global.NAMESPACE, Global.METHOD_NAME_GETSERVICELEVELAGREEMENT);
//        request.addProperty("filters", json.toString());
//
//        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        envelope.dotNet = true;
//        envelope.setOutputSoapObject(request);
//
//        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
//        try {
//            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Global.SOAP_ACTION_GETSERVICELEVELAGREEMENT), envelope);
//
//            SoapObject result = (SoapObject) envelope.getResponse();
//            if (result != null && result.getPropertyCount() > 0) {
//                SoapObject closingRateSoap = (SoapObject) result.getProperty("ClosingRate");
//                if (closingRateSoap != null) {
//                    ClosingRate closingRate = new ClosingRate();
//                    closingRate.setQualified(Integer.parseInt(closingRateSoap.getProperty("Qualified").toString()));
//                    closingRate.setTarget(Integer.parseInt(closingRateSoap.getProperty("Target").toString()));
//                    closingRate.setClosedWon(Integer.parseInt(closingRateSoap.getProperty("ClosedWon").toString()));
//                    closingRate.setClosedWonPct(Integer.parseInt(closingRateSoap.getProperty("ClosedWonPct").toString()));
//                    closingRate.setClosedWonCC(Integer.parseInt(closingRateSoap.getProperty("ClosedWonCC").toString()));
//                    closingRate.setClosedWonCCPct(Integer.parseInt(closingRateSoap.getProperty("ClosedWonCCPct").toString()));
//                    closingRate.setClosedWonWI(Integer.parseInt(closingRateSoap.getProperty("ClosedWonWI").toString()));
//                    closingRate.setClosedWonWIPct(Integer.parseInt(closingRateSoap.getProperty("ClosedWonWIPct").toString()));
//                    closingRate.setClosedWonGraph(closingRateSoap.getProperty("ClosedWonGraph").toString());
//                    sla.setClosingRate(closingRate);
//                } else {
//                    sla.setClosingRate(new ClosingRate());
//                }
//
//                SoapObject vocSoap = (SoapObject) result.getProperty("VoiceOfCustomer");
//                if (vocSoap != null) {
//                    VoiceOfCustomer voc = new VoiceOfCustomer();
//                    voc.setActualVocScore(Integer.parseInt(vocSoap.getProperty("ActualVocScore").toString()));
//                    voc.setMaxVocScore(Integer.parseInt(vocSoap.getProperty("MaxVocScore").toString()));
//                    voc.setVocTarget(Integer.parseInt(vocSoap.getProperty("VocTarget").toString()));
//                    voc.setVocAverage(Integer.parseInt(vocSoap.getProperty("VocAverage").toString()));
//                    voc.setVocTodayPct(Integer.parseInt(vocSoap.getProperty("VocTodayPct").toString()));
//                    voc.setVocGraph(vocSoap.getProperty("VocGraph").toString());
//                    sla.setVoc(voc);
//                } else {
//                    sla.setVoc(new VoiceOfCustomer());
//                }
//
//                SoapObject attSoap = (SoapObject) result.getProperty("AttendanceRate");
//                if (attSoap != null) {
//                    AttendanceRate att = new AttendanceRate();
//                    att.setBookedCC(Integer.parseInt(attSoap.getProperty("BookedCC").toString()));
//                    att.setAttendedCC(Integer.parseInt(attSoap.getProperty("AttendedCC").toString()));
//                    att.setAttendedCCPct(Integer.parseInt(attSoap.getProperty("AttendedCCPct").toString()));
//                    att.setAttendedTodayCCPct(Integer.parseInt(attSoap.getProperty("AttendedTodayCCPct").toString()));
//                    att.setAttendedTarget(Integer.parseInt(attSoap.getProperty("AttendedTarget").toString()));
//                    att.setAttendedGraph(attSoap.getProperty("AttendedGraph").toString());
//                    sla.setAttendanceRate(att);
//                } else {
//                    sla.setAttendanceRate(new AttendanceRate());
//                }
//
//                SoapObject qualSoap = (SoapObject) result.getProperty("QualificationRate");
//                if (qualSoap != null) {
//                    QualificationRate qual = new QualificationRate();
//                    qual.setQualificationTarget(Integer.parseInt(qualSoap.getProperty("QualifiedTarget").toString()));
//                    qual.setQualified(Integer.parseInt(qualSoap.getProperty("Qualified").toString()));
//                    qual.setQualifiedPct(Integer.parseInt(qualSoap.getProperty("QualifiedPct").toString()));
//                    qual.setNonQualified(Integer.parseInt(qualSoap.getProperty("NonQualified").toString()));
//                    qual.setQualifiedTodayPct(Integer.parseInt(qualSoap.getProperty("QualifiedTodayPct").toString()));
//                    qual.setQualifiedGraph(qualSoap.getProperty("QualifiedGraph").toString());
//                    sla.setQualificationRate(qual);
//                } else {
//                    sla.setQualificationRate(new QualificationRate());
//                }
//
//                SoapObject qualitySoap = (SoapObject) result.getProperty("Quality");
//                if (qualitySoap != null) {
//                    Quality quality = new Quality();
//                    quality.setQaTarget(Integer.parseInt(qualitySoap.getProperty("QATarget").toString()));
//                    quality.setQcTarget(Integer.parseInt(qualitySoap.getProperty("QCTarget").toString()));
//                    quality.setQaqcTarget(Integer.parseInt(qualitySoap.getProperty("QAQCTarget").toString()));
//                    quality.setQa(Integer.parseInt(qualitySoap.getProperty("QA").toString()));
//                    quality.setQc(Integer.parseInt(qualitySoap.getProperty("QC").toString()));
//                    quality.setQaqcTotal(Integer.parseInt(qualitySoap.getProperty("QAQCTotal").toString()));
//                    quality.setQaPct(Integer.parseInt(qualitySoap.getProperty("QAPct").toString()));
//                    quality.setQcPct(Integer.parseInt(qualitySoap.getProperty("QCPct").toString()));
//                    quality.setQaqcPct(Integer.parseInt(qualitySoap.getProperty("QAQCPct").toString()));
//                    sla.setQuality(quality);
//                } else {
//                    sla.setQuality(new Quality());
//                }
//
//            }
//        } catch(Exception e) {
//
//            sla.setClosingRate(new ClosingRate());
//            sla.setVoc(new VoiceOfCustomer());
//            sla.setAttendanceRate(new AttendanceRate());
//            sla.setQualificationRate(new QualificationRate());
//            sla.setQuality(new Quality());
//            e.printStackTrace();
//        }

        ClosingRate closingRate = new ClosingRate();
        closingRate.setQualified(286);
        closingRate.setTarget(10);
        closingRate.setClosedWon(33);
        closingRate.setClosedWonPct(30);
        closingRate.setClosedWonCC(12);
        closingRate.setClosedWonCCPct(27);
        closingRate.setClosedWonWI(21);
        closingRate.setClosedWonWIPct(33);
        closingRate.setClosedWonGraph("11-01-2015=3=5=DATERANGE,11-02-2015=1=7=DATERANGE,11-03-2015=3=5=DATERANGE,11-04-2015=3=8=DATERANGE,11-05-2015=1=5=DATERANGE,11-07-2015=5=18=DATERANGE,11-08-2015=1=4=DATERANGE,11-09-2015=4=9=DATERANGE,11-10-2015=2=7=DATERANGE,11-11-2015=3=14=DATERANGE,11-12-2015=2=4=DATERANGE,11-14-2015=3=10=DATERANGE,11-15-2015=2=15=DATERANGE,");
        sla.setClosingRate(closingRate);

        VoiceOfCustomer voc = new VoiceOfCustomer();
        voc.setActualVocScore(4325);
        voc.setMaxVocScore(4900);
        voc.setVocTarget(90);
        voc.setVocAverage(89);
        voc.setVocTodayPct(0);
        voc.setVocGraph("11-03-2015=1050=1100=DATERANGE,11-04-2015=1125=1300=DATERANGE,11-05-2015=75=100=DATERANGE,11-06-2015=350=400=DATERANGE,11-07-2015=575=600=DATERANGE,11-10-2015=550=600=DATERANGE,11-11-2015=600=800=DATERANGE,");
        sla.setVoc(voc);

        AttendanceRate att = new AttendanceRate();
        att.setBookedCC(114);
        att.setAttendedCC(46);
        att.setAttendedCCPct(41);
        att.setAttendedTodayCCPct(0);
        att.setAttendedTarget(30);
        att.setAttendedGraph("11-01-2015=2=6=DATERANGE,11-02-2015=3=3=DATERANGE,11-03-2015=2=9=DATERANGE,11-04-2015=4=5=DATERANGE,11-05-2015=5=5=DATERANGE,11-07-2015=4=14=DATERANGE,11-08-2015=1=7=DATERANGE,11-09-2015=4=12=DATERANGE,11-10-2015=3=12=DATERANGE,11-11-2015=8=11=DATERANGE,11-12-2015=4=6=DATERANGE,11-14-2015=1=12=DATERANGE,11-15-2015=5=12=DATERANGE,");
        sla.setAttendanceRate(att);

        QualificationRate qual = new QualificationRate();
        qual.setQualificationTarget(50);
        qual.setQualified(286);
        qual.setQualifiedPct(38);
        qual.setNonQualified(757);
        qual.setQualifiedTodayPct(0);
        qual.setQualifiedGraph("11-01-2015=15=47=DATERANGE,11-02-2015=13=54=DATERANGE,11-03-2015=17=42=DATERANGE,11-04-2015=20=66=DATERANGE,11-05-2015=15=41=DATERANGE,11-07-2015=40=75=DATERANGE,11-08-2015=17=59=DATERANGE,11-09-2015=24=58=DATERANGE,11-10-2015=22=48=DATERANGE,11-11-2015=27=73=DATERANGE,11-12-2015=14=44=DATERANGE,11-14-2015=25=76=DATERANGE,11-15-2015=37=74=DATERANGE,");
        sla.setQualificationRate(qual);

        Quality quality = new Quality();
        quality.setQaTarget(75);
        quality.setQcTarget(90);
        quality.setQaqcTarget(80);
        quality.setQa(139);
        quality.setQc(292);
        quality.setQaqcTotal(430);
        quality.setQaPct(70);
        quality.setQcPct(98);
        quality.setQaqcPct(84);
        sla.setQuality(quality);

        return sla;
    }

    @Override
    protected void onPostExecute(ServiceLevelAgreement sla) {
        callback.getResult(sla);
    }
}
