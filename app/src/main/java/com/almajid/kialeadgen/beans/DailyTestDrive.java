package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 11/22/15.
 */
public class DailyTestDrive {

    private int totalBookings;
    private int attended;
    private int attendedCC;
    private int attendedWI;
    private int notAttended;
    private String graph;

    public int getTotalBookings() {
        return totalBookings;
    }

    public void setTotalBookings(int totalBookings) {
        this.totalBookings = totalBookings;
    }

    public int getAttended() {
        return attended;
    }

    public void setAttended(int attended) {
        this.attended = attended;
    }

    public int getAttendedCC() {
        return attendedCC;
    }

    public void setAttendedCC(int attendedCC) {
        this.attendedCC = attendedCC;
    }

    public int getAttendedWI() {
        return attendedWI;
    }

    public void setAttendedWI(int attendedWI) {
        this.attendedWI = attendedWI;
    }

    public int getNotAttended() {
        return notAttended;
    }

    public void setNotAttended(int notAttended) {
        this.notAttended = notAttended;
    }

    public String getGraph() {
        return graph;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }
}
