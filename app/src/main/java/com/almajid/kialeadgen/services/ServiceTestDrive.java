package com.almajid.kialeadgen.services;

import android.content.Context;
import android.os.AsyncTask;

import com.almajid.kialeadgen.beans.ContactType;
import com.almajid.kialeadgen.beans.Contacts;
import com.almajid.kialeadgen.beans.DailyContacts;
import com.almajid.kialeadgen.beans.DailyTestDrive;
import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.beans.TestDrive;
import com.almajid.kialeadgen.interfaces.ContactsImpl;
import com.almajid.kialeadgen.interfaces.TestDriveImpl;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by relly on 11/21/15.
 */
public class ServiceTestDrive extends AsyncTask<Void, Void, TestDrive> {


    private Context context;
    private TestDriveImpl callback;
    private LeadSharedPreferences prefs;

    public ServiceTestDrive(Context context, TestDriveImpl callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected TestDrive doInBackground(Void... params) {

        prefs = new LeadSharedPreferences(context);
        Filter filter = prefs.getFilter();
        JSONObject json = new JSONObject();
        try {
            json.put("CodeId", 2);
            json.put("All", filter.isAll());
            json.put("Voice", filter.isVoice());
            json.put("Chat", filter.isLiveChat());
            json.put("Email", filter.isEmail());
            json.put("SocialMedia", filter.isSocialMedia());
            json.put("Walkin", filter.isWalkin());
            json.put("Individual", filter.isIndividual());
            json.put("Luxury", filter.isLuxury());
            json.put("DateRange", filter.getDateRange());
            json.put("Monthly", filter.getMonthly());
            json.put("MTD", filter.isMtd());
        } catch (JSONException e) {
            return null;
        }


        TestDrive testDrive = new TestDrive();

//        SoapObject request = new SoapObject(Global.NAMESPACE, Global.METHOD_NAME_GETTESTDRIVE);
//        request.addProperty("filters", json.toString());
//
//        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        envelope.dotNet = true;
//        envelope.setOutputSoapObject(request);
//
//        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
//        try {
//            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Global.SOAP_ACTION_GETTESTDRIVE), envelope);
//
//            SoapObject result = (SoapObject) envelope.getResponse();
//            if (result != null && result.getPropertyCount() > 0) {
//                SoapObject dtsoap = (SoapObject) result.getProperty("DailyTestDrive");
//                if (dtsoap != null) {
//                    DailyTestDrive dt = new DailyTestDrive();
//                    dt.setTotalBookings(Integer.parseInt(dtsoap.getProperty("TotalBookings").toString()));
//                    dt.setAttended(Integer.parseInt(dtsoap.getProperty("Attended").toString()));
//                    dt.setAttendedCC(Integer.parseInt(dtsoap.getProperty("AttendedCC").toString()));
//                    dt.setAttendedWI(Integer.parseInt(dtsoap.getProperty("AttendedWI").toString()));
//                    dt.setNotAttended(Integer.parseInt(dtsoap.getProperty("NotAttended").toString()));
//                    dt.setGraph(dtsoap.getProperty("Graph").toString());
//                    testDrive.setDailyTestDrive(dt);
//                } else {
//                    testDrive.setDailyTestDrive(new DailyTestDrive());
//                }
//
//                SoapObject ctsoap = (SoapObject) result.getProperty("ContactType");
//                if (ctsoap != null) {
//                    ContactType ct = new ContactType();
//                    ct.setTotal(Integer.parseInt(ctsoap.getProperty("Total").toString()));
//                    ct.setIndividual(Integer.parseInt(ctsoap.getProperty("Individual").toString()));
//                    ct.setLuxury(Integer.parseInt(ctsoap.getProperty("Luxury").toString()));
//                    ct.setIndividualPct(Integer.parseInt(ctsoap.getProperty("IndividualPct").toString()));
//                    ct.setLuxuryPct(Integer.parseInt(ctsoap.getProperty("LuxuryPct").toString()));
//                    testDrive.setContactType(ct);
//                } else {
//                    testDrive.setContactType(new ContactType());
//                }
//
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            testDrive.setDailyTestDrive(new DailyTestDrive());
//            testDrive.setContactType(new ContactType());
//        }


        DailyTestDrive dt = new DailyTestDrive();
        dt.setTotalBookings(140);
        dt.setAttended(100);
        dt.setAttendedCC(35);
        dt.setAttendedWI(65);
        dt.setNotAttended(40);
        dt.setGraph("11-01-2015=5=DATERANGE,11-02-2015=10=DATERANGE,11-03-2015=8=DATERANGE,11-04-2015=12=DATERANGE,11-05-2015=10=DATERANGE,11-07-2015=19=DATERANGE,11-08-2015=10=DATERANGE,11-09-2015=10=DATERANGE,11-10-2015=7=DATERANGE,11-11-2015=15=DATERANGE,11-12-2015=6=DATERANGE,11-14-2015=12=DATERANGE,11-15-2015=16=DATERANGE,");
        testDrive.setDailyTestDrive(dt);

        ContactType ct = new ContactType();
        ct.setTotal(217);
        ct.setIndividual(198);
        ct.setLuxury(19);
        ct.setIndividualPct(98);
        ct.setLuxuryPct(2);
        testDrive.setContactType(ct);

        return testDrive;
    }

    @Override
    protected void onPostExecute(TestDrive testDrive) {
        callback.getResult(testDrive);
    }
}
