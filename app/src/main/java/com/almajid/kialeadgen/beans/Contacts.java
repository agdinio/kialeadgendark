package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 11/21/15.
 */
public class Contacts {

    private DailyContacts dailyContacts;

    public DailyContacts getDailyContacts() {
        return dailyContacts;
    }

    public void setDailyContacts(DailyContacts dailyContacts) {
        this.dailyContacts = dailyContacts;
    }
}
