package com.almajid.kialeadgen;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.almajid.kialeadgen.beans.Contacts;
import com.almajid.kialeadgen.beans.TestDrive;
import com.almajid.kialeadgen.interfaces.ContactsImpl;
import com.almajid.kialeadgen.interfaces.TestDriveImpl;
import com.almajid.kialeadgen.services.ServiceContacts;
import com.almajid.kialeadgen.services.ServiceTestDrive;
import com.androidpagecontrol.PageControl;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by relly on 11/21/15.
 */
public class FragmentTestDrive extends Fragment {

    private View rootView;
    private ProgressBar progress;
    private PagerAdapter mPageAdapter;
    private ViewPager mPager;
    private List<Class> fragments;

    private FragmentTestDrivep1 p1;
    private FragmentTestDrivep2 p2;

    @SuppressLint("NewApi")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_testdrive));

        rootView = inflater.inflate(R.layout.fragment_testdrive, container, false);
        progress = (ProgressBar) rootView.findViewById(R.id.progress);
        progress.setVisibility(View.VISIBLE);

        new ServiceTestDrive(rootView.getContext(),
                new TestDriveImpl() {
                    @Override
                    public void getResult(TestDrive testDrive) {
                        initFragments(testDrive);
                        progress.setVisibility(View.INVISIBLE);
                    }
                }).execute();

        getActivity().invalidateOptionsMenu();

        return rootView;
    }


    private void initFragments(TestDrive testDrive) {
        fragments = new ArrayList<>();
        fragments.add(FragmentTestDrivep1.class);
        fragments.add(FragmentTestDrivep2.class);

        mPageAdapter = new PageAdapter(getActivity().getSupportFragmentManager(), testDrive);
        mPager = (ViewPager) rootView.findViewById(R.id.pageContainer);
        mPager.setAdapter(mPageAdapter);
        mPager.setOffscreenPageLimit(0);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        if (p1 != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    p1.updateLabels();
                                    p1.updateChart();
                                }
                            }, 500);
                        }
                        break;
                    case 1:
                        if (p2 != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    p2.updateLabels();
                                    p2.updateChart();
                                }
                            }, 500);
                        }
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        PageControl pageControl = (PageControl) rootView.findViewById(R.id.page_control);
        pageControl.setViewPager(mPager);
        pageControl.setPosition(0);

    }


    private class PageAdapter extends FragmentStatePagerAdapter {

        private TestDrive testDrive;

        public PageAdapter(FragmentManager fm, TestDrive testDrive) {
            super(fm);
            this.testDrive = testDrive;
        }

        @Override
        public Fragment getItem(int position) {
                switch(position) {
                case 0:
                    p1 = new FragmentTestDrivep1();
                    p1.setTestDrive(testDrive);
                    return p1;
                case 1:
                    p2 = new FragmentTestDrivep2();
                    p2.setTestDrive(testDrive);
                    return p2;
                default:
                    p1 = new FragmentTestDrivep1();
                    return p1;
            }

        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

//        @Override
//        public boolean isViewFromObject(View view, Object object) {
//            if (object != null) {
//                return ((Fragment)object).getView() == view;
//            } else {
//                return false;
//            }
//        }

        @Override
        public int getCount() {
            return fragments.size();
        }

    }

}
