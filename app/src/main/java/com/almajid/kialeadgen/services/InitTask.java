package com.almajid.kialeadgen.services;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by relly on 7/26/15.
 */
public class InitTask extends AsyncTask<Context, Integer, String> {


    @Override
    protected String doInBackground(Context... params) {
        int i = 0;
        while( i <= 50 )
        {
            try{
                Thread.sleep(50);
                publishProgress( i );
                i++;
            } catch( Exception e ){
                Log.i("makemachine", (e.getMessage() == null) ? e.getMessage() : e.toString());
            }
        }
        return "COMPLETE!";
    }

    @Override
    protected void onPreExecute() {
        Log.i("makemachine", "onPreExecute()");
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values)
    {
        super.onProgressUpdate(values);
        Log.i("makemachine", "onProgressUpdate(): " + String.valueOf(values[0]));
    }

    @Override
    protected void onCancelled()
    {
        super.onCancelled();
        Log.i("makemachine", "onCancelled()");
    }

    @Override
    protected void onPostExecute( String result )
    {
        super.onPostExecute(result);
        Log.i("makemachine", "onPostExecute(): " + result);
        //_cancelButton.setVisibility( View.INVISIBLE );
    }

}
