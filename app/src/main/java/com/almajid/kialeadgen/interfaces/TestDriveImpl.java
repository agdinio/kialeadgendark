package com.almajid.kialeadgen.interfaces;

import com.almajid.kialeadgen.beans.Contacts;
import com.almajid.kialeadgen.beans.TestDrive;

/**
 * Created by relly on 11/22/15.
 */
public interface TestDriveImpl {

    void getResult(TestDrive testDrive);
}
