package com.almajid.kialeadgen;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.ClosingRate;
import com.almajid.kialeadgen.beans.ServiceLevelAgreement;
import com.almajid.kialeadgen.beans.TestDriveLeads;
import com.almajid.kialeadgen.beans.TestDriveLeadsStatus;
import com.almajid.kialeadgen.beans.VoiceOfCustomer;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentLeadsp2 extends Fragment {


    private TestDriveLeads leads;
    private View rootView;
    private LinearLayout content_frame_nav;

    private TextView txtTotal;

    private TextView txtOpen;
    private TextView txtOpenCCSO;
    private TextView txtOpenCCWSO;
    private TextView txtOpenWISO;
    private TextView txtOpenWIWSO;

    private TextView txtWon;
    private TextView txtWonCC;
    private TextView txtWonWI;

    private TextView txtLoss;
    private TextView txtLossCC;
    private TextView txtLossWI;

    private TextView txtDeadlead;
    private TextView txtDeadleadCCAtt;
    private TextView txtDeadleadWIAtt;
    private TextView txtDeadleadNotAtt;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (leads != null) {
            rootView = inflater.inflate(R.layout.fragment_leads_p2, container, false);

            txtTotal = (TextView) rootView.findViewById(R.id.txtTotal);
            txtOpen = (TextView) rootView.findViewById(R.id.txtOpen);
            txtOpenCCSO = (TextView) rootView.findViewById(R.id.txtOpenCCSO);
            txtOpenCCWSO = (TextView) rootView.findViewById(R.id.txtOpenCCWSO);
            txtOpenWISO = (TextView) rootView.findViewById(R.id.txtOpenWISO);
            txtOpenWIWSO = (TextView) rootView.findViewById(R.id.txtOpenWIWSO);
            txtWon = (TextView) rootView.findViewById(R.id.txtWon);
            txtWonCC = (TextView) rootView.findViewById(R.id.txtWonCC);
            txtWonWI = (TextView) rootView.findViewById(R.id.txtWonWI);
            txtLoss = (TextView) rootView.findViewById(R.id.txtLoss);
            txtLossCC = (TextView) rootView.findViewById(R.id.txtLossCC);
            txtLossWI = (TextView) rootView.findViewById(R.id.txtLossWI);
            txtDeadlead = (TextView) rootView.findViewById(R.id.txtDeadlead);
            txtDeadleadCCAtt = (TextView) rootView.findViewById(R.id.txtDeadleadCCAtt);
            txtDeadleadWIAtt = (TextView) rootView.findViewById(R.id.txtDeadleadWIAtt);
            txtDeadleadNotAtt = (TextView) rootView.findViewById(R.id.txtDeadleadNotAtt);

        } else {
            Toast.makeText(getActivity().getBaseContext(), "Data cannot be retrieved. Please try again.", Toast.LENGTH_LONG).show();
        }

/*
        content_frame_nav = (LinearLayout) ((FragmentActivity) rootView.getContext()).findViewById(R.id.content_frame_nav);
        content_frame_nav.setVisibility(View.GONE);
*/


        return rootView;
    }

    public void setLeads(TestDriveLeads leads) {
        this.leads = leads;
    }

    public void refresh() {

        TestDriveLeadsStatus status = leads.getTestDriveLeadsStatus();

        txtTotal.setText(String.valueOf(status.getTotal()));

        txtOpen.setText(String.valueOf(status.getOpen()));
        txtOpenCCSO.setText(String.valueOf(status.getOpenccso()));
        txtOpenCCWSO.setText(String.valueOf(status.getOpenccwso()));
        txtOpenWISO.setText(String.valueOf(status.getOpenwiso()));
        txtOpenWIWSO.setText(String.valueOf(status.getOpenwiwso()));
        txtWon.setText(String.valueOf(status.getWon()));
        txtWonCC.setText(String.valueOf(status.getWoncc()));
        txtWonWI.setText(String.valueOf(status.getWonwi()));
        txtLoss.setText(String.valueOf(status.getLoss()));
        txtLossCC.setText(String.valueOf(status.getLosscc()));
        txtLossWI.setText(String.valueOf(status.getLosswi()));
        txtDeadlead.setText(String.valueOf(status.getDeadlead()));
        txtDeadleadCCAtt.setText(String.valueOf(status.getDeadleadattcc()));
        txtDeadleadWIAtt.setText(String.valueOf(status.getDeadleadattwi()));
        txtDeadleadNotAtt.setText(String.valueOf(status.getDeadleadnotatt()));
    }

}
