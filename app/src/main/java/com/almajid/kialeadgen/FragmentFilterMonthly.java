package com.almajid.kialeadgen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.helpers.DropdownYear;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentFilterMonthly extends Fragment {

    private LeadSharedPreferences prefs;

    private View rootView;
    private Button btnSave;

    private CheckBox jan;
    private CheckBox feb;
    private CheckBox mar;
    private CheckBox apr;
    private CheckBox may;
    private CheckBox jun;
    private CheckBox jul;
    private CheckBox aug;
    private CheckBox sep;
    private CheckBox oct;
    private CheckBox nov;
    private CheckBox dec;
    private Spinner cboYear;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_filter_monthly, container, false);

        jan = (CheckBox) rootView.findViewById(R.id.jan);
        feb = (CheckBox) rootView.findViewById(R.id.feb);
        mar = (CheckBox) rootView.findViewById(R.id.mar);
        apr = (CheckBox) rootView.findViewById(R.id.apr);
        may = (CheckBox) rootView.findViewById(R.id.may);
        jun = (CheckBox) rootView.findViewById(R.id.jun);
        jul = (CheckBox) rootView.findViewById(R.id.jul);
        aug = (CheckBox) rootView.findViewById(R.id.aug);
        sep = (CheckBox) rootView.findViewById(R.id.sep);
        oct = (CheckBox) rootView.findViewById(R.id.oct);
        nov = (CheckBox) rootView.findViewById(R.id.nov);
        dec = (CheckBox) rootView.findViewById(R.id.dec);
        cboYear = (Spinner) rootView.findViewById(R.id.cboYear);

        Calendar cbeginyear = Calendar.getInstance();
        Calendar cendyear = Calendar.getInstance();
        cbeginyear.set(2015, 01, 01);
        cendyear.setTime(new Date());

        List<String> list = new ArrayList<>();
        for (int i=cbeginyear.get(Calendar.YEAR); i<=cendyear.get(Calendar.YEAR); i++) {
            list.add(String.valueOf(i));
        }

        ArrayAdapter<String> spinAdapter = new ArrayAdapter<String>(rootView.getContext(), R.layout.dropdown_year, list);
        cboYear.setAdapter(spinAdapter);

        btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder str = new StringBuilder();

                if (jan.isChecked())
                {
                    str.append("01-01-" + cboYear.getSelectedItem() + ",");
                }

                if (feb.isChecked())
                    str.append("02-01-" + cboYear.getSelectedItem() + ",");

                if (mar.isChecked())
                    str.append("03-01-" + cboYear.getSelectedItem() + ",");

                if (apr.isChecked())
                    str.append("04-01-" + cboYear.getSelectedItem() + ",");

                if (may.isChecked())
                    str.append("05-01-" + cboYear.getSelectedItem() + ",");

                if (jun.isChecked())
                    str.append("06-01-" + cboYear.getSelectedItem() + ",");

                if (jul.isChecked())
                    str.append("07-01-" + cboYear.getSelectedItem() + ",");

                if (aug.isChecked())
                    str.append("08-01-" + cboYear.getSelectedItem() + ",");

                if (sep.isChecked())
                    str.append("09-01-" + cboYear.getSelectedItem() + ",");

                if (oct.isChecked())
                    str.append("10-01-" + cboYear.getSelectedItem() + ",");

                if (nov.isChecked())
                    str.append("11-01-" + cboYear.getSelectedItem() + ",");

                if (dec.isChecked())
                    str.append("12-01-" + cboYear.getSelectedItem() + ",");

                if (!TextUtils.isEmpty(str.toString().trim())) {

                    str.setLength(str.length() - 1);
                    prefs = new LeadSharedPreferences(rootView.getContext());
                    Filter filter = prefs.getFilter();
                    if (filter == null) {

                        filter = new Filter();
                        filter.setVoice(false);
                        filter.setEmail(false);
                        filter.setLiveChat(false);
                        filter.setSocialMedia(false);
                        filter.setWalkin(false);
                        filter.setAll(true);
                        filter.setIndividual(true);
                        filter.setLuxury(true);

                        filter.setDateRange("");
                        filter.setMonthly(str.toString());
                        filter.setMtd(false);
                        filter.setFilterType("MONTHLY");

                        String jsonString = prefs.toJson(filter);
                        prefs.editMode();
                        prefs.putString("FILTER", jsonString);
                        prefs.commitMode();
                    } else {

                        filter.setDateRange("");
                        filter.setMonthly(str.toString());
                        filter.setMtd(false);
                        filter.setFilterType("MONTHLY");

                        String jsonString = prefs.toJson(filter);
                        prefs.editMode();
                        prefs.putString("FILTER", jsonString);
                        prefs.commitMode();
                    }

                    new AlertDialog.Builder(rootView.getContext())
                            .setCancelable(false)
                            .setTitle("Saved")
                            .setMessage("MONTH HAS BEEN SET")
                            .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .create()
                            .show();


                } else {

                    new AlertDialog.Builder(rootView.getContext())
                            .setCancelable(false)
                            .setTitle("No Date Range")
                            .setMessage("PLEASE SELECT A MONTH")
                            .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .create()
                            .show();

                }
            }
        });

        return rootView;
    }
}
