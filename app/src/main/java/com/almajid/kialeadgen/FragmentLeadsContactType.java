package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentLeadsContactType extends Fragment {


    private View rootView;
    private PieChart leadsChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_leads_contact_type, container, false);
        leadsChart = (PieChart) rootView.findViewById(R.id.leadsChart);

        return rootView;
    }

    public void updateChart() {

        leadsChart.setUsePercentValues(true);
        leadsChart.setDescription("");
        leadsChart.setDragDecelerationFrictionCoef(0.95f);

        leadsChart.setDrawHoleEnabled(false);
        leadsChart.setHoleColorTransparent(true);

        leadsChart.setTransparentCircleColor(Color.WHITE);
        leadsChart.setTransparentCircleAlpha(110);

        leadsChart.setHoleRadius(58f);
        leadsChart.setTransparentCircleRadius(61f);

        leadsChart.setDrawCenterText(true);
        leadsChart.setDrawSliceText(false);

        leadsChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        leadsChart.setRotationEnabled(false);
        leadsChart.animateY(1500, Easing.EasingOption.EaseInOutQuad);

        Legend l = leadsChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_CENTER);
        l.setTextColor(Color.WHITE);
        l.setTextSize(8f);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        float mult = 100;
        int count = 2;

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        yVals1.add(new Entry(87, 0));
        yVals1.add(new Entry(13, 1));


        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("INDIVIDUAL");
        xVals.add("LUXURY");

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(5f);



        ArrayList<Integer> colors = new ArrayList<Integer>();
        colors.add(Color.parseColor("#162d46"));
        colors.add(Color.parseColor("#461C1F"));


        dataSet.setColors(colors);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(8f);
        data.setValueTextColor(Color.WHITE);

        leadsChart.setData(data);

        // undo all highlights
        leadsChart.highlightValues(null);

        leadsChart.invalidate();

    }

}
