package com.almajid.kialeadgen.drawer;

/**
 * Created by relly on 7/1/15.
 */
public class NavMenuSection implements NavDrawerItemImpl {

    public static final int SECTION_TYPE = 0;
    private int id;
    private String label;
    private String name;

    public NavMenuSection(){}

    public static NavMenuSection create(int id, String label, String name) {
        NavMenuSection section = new NavMenuSection();
        section.setLabel(label);
        section.setName(name);

        return section;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getType() {
        return SECTION_TYPE;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public boolean updateActionBarTitle() {
        return false;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setName(String name) {
        this.name = name;
    }
}
