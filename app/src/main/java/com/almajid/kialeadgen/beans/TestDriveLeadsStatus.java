package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 11/23/15.
 */
public class TestDriveLeadsStatus {

    private int total;

    private int open;
    private int openccso;
    private int openccwso;
    private int openwiso;
    private int openwiwso;

    private int won;
    private int woncc;
    private int wonwi;

    private int loss;
    private int losscc;
    private int losswi;

    private int deadlead;
    private int deadleadattcc;
    private int deadleadattwi;
    private int deadleadnotatt;

    private int openpct;
    private int wonpct;
    private int losspct;
    private int deadleadpct;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getOpen() {
        return open;
    }

    public void setOpen(int open) {
        this.open = open;
    }

    public int getOpenccso() {
        return openccso;
    }

    public void setOpenccso(int openccso) {
        this.openccso = openccso;
    }

    public int getOpenccwso() {
        return openccwso;
    }

    public void setOpenccwso(int openccwso) {
        this.openccwso = openccwso;
    }

    public int getOpenwiso() {
        return openwiso;
    }

    public void setOpenwiso(int openwiso) {
        this.openwiso = openwiso;
    }

    public int getOpenwiwso() {
        return openwiwso;
    }

    public void setOpenwiwso(int openwiwso) {
        this.openwiwso = openwiwso;
    }

    public int getWon() {
        return won;
    }

    public void setWon(int won) {
        this.won = won;
    }

    public int getWoncc() {
        return woncc;
    }

    public void setWoncc(int woncc) {
        this.woncc = woncc;
    }

    public int getWonwi() {
        return wonwi;
    }

    public void setWonwi(int wonwi) {
        this.wonwi = wonwi;
    }

    public int getLoss() {
        return loss;
    }

    public void setLoss(int loss) {
        this.loss = loss;
    }

    public int getLosscc() {
        return losscc;
    }

    public void setLosscc(int losscc) {
        this.losscc = losscc;
    }

    public int getLosswi() {
        return losswi;
    }

    public void setLosswi(int losswi) {
        this.losswi = losswi;
    }

    public int getDeadlead() {
        return deadlead;
    }

    public void setDeadlead(int deadlead) {
        this.deadlead = deadlead;
    }

    public int getDeadleadattcc() {
        return deadleadattcc;
    }

    public void setDeadleadattcc(int deadleadattcc) {
        this.deadleadattcc = deadleadattcc;
    }

    public int getDeadleadattwi() {
        return deadleadattwi;
    }

    public void setDeadleadattwi(int deadleadattwi) {
        this.deadleadattwi = deadleadattwi;
    }

    public int getDeadleadnotatt() {
        return deadleadnotatt;
    }

    public void setDeadleadnotatt(int deadleadnotatt) {
        this.deadleadnotatt = deadleadnotatt;
    }

    public int getOpenpct() {
        return openpct;
    }

    public void setOpenpct(int openpct) {
        this.openpct = openpct;
    }

    public int getWonpct() {
        return wonpct;
    }

    public void setWonpct(int wonpct) {
        this.wonpct = wonpct;
    }

    public int getLosspct() {
        return losspct;
    }

    public void setLosspct(int losspct) {
        this.losspct = losspct;
    }

    public int getDeadleadpct() {
        return deadleadpct;
    }

    public void setDeadleadpct(int deadleadpct) {
        this.deadleadpct = deadleadpct;
    }
}
