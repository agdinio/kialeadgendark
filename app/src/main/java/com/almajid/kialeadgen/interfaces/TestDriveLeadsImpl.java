package com.almajid.kialeadgen.interfaces;

import com.almajid.kialeadgen.beans.TestDrive;
import com.almajid.kialeadgen.beans.TestDriveLeads;

/**
 * Created by relly on 11/22/15.
 */
public interface TestDriveLeadsImpl {

    void getResult(TestDriveLeads leads);
}
