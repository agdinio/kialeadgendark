package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentLeadsBranchwise extends Fragment {


    private View rootView;
    private BarChart leadsChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_leads_branchwise, container, false);
        leadsChart = (BarChart) rootView.findViewById(R.id.leadsChart);

        return rootView;
    }

    public void updateChart() {

        leadsChart.setDescription("");
        leadsChart.setPinchZoom(false);
        leadsChart.setDrawBarShadow(false);
        leadsChart.setDrawGridBackground(false);
        leadsChart.getAxisLeft().setDrawGridLines(false);
        leadsChart.animateY(2500);
        leadsChart.getAxisRight().setDrawLabels(false);
        leadsChart.getAxisLeft().setDrawLabels(false);
        leadsChart.getLegend().setEnabled(false);

        XAxis xAxis = leadsChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(8f);
        xAxis.setSpaceBetweenLabels(0);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));

        YAxis lyaxis = leadsChart.getAxisLeft();
        lyaxis.setTextSize(8f);
        lyaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));

        ArrayList<String> xVals = new ArrayList<>();
        xVals.add("MAC");
        xVals.add("DXB");
        xVals.add("AUH");
        xVals.add("SHJ");
        xVals.add("MUS");
        xVals.add("AJM");
        xVals.add("AI N");
        xVals.add("FUJ");

        ArrayList<BarEntry> yVals = new ArrayList<>();
        BarEntry barEntry = new BarEntry(203, 0);
        yVals.add(barEntry);

        barEntry = new BarEntry(120, 1);
        yVals.add(barEntry);
        barEntry = new BarEntry(81, 2);
        yVals.add(barEntry);
        barEntry = new BarEntry(51, 3);
        yVals.add(barEntry);
        barEntry = new BarEntry(25, 4);
        yVals.add(barEntry);
        barEntry = new BarEntry(18, 5);
        yVals.add(barEntry);
        barEntry = new BarEntry(40, 6);
        yVals.add(barEntry);
        barEntry = new BarEntry(3, 7);
        yVals.add(barEntry);

        BarDataSet barDataSet = new BarDataSet(yVals, "Branchwise");
        barDataSet.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        barDataSet.setColor(Color.YELLOW);
        barDataSet.setDrawValues(true);
        barDataSet.setValueTextSize(8f);

        BarData data = new BarData(xVals, barDataSet);

        leadsChart.setData(data);
        leadsChart.invalidate();


    }

}
