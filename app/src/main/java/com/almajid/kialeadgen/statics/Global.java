package com.almajid.kialeadgen.statics;

/**
 * Created by relly on 9/7/15.
 */
public class Global {

    public static String FRAG_WELCOME = "FRAG_WELCOME";
    public static String FRAG_FILTER_DATE = "FRAG_FILTER_DATE";
    public static String FRAG_SLA = "FRAG_SLA";
    public static String FRAG_CONTACTS = "FRAG_CONTACTS";
    public static String FRAG_TESTDRIVE = "FRAG_TESTDRIVE";
    public static String FRAG_LEADS = "FRAG_LEADS";

    public static String NAMESPACE = "http://tempuri.org/";
    public static String SERVICE_URL = "http://192.168.56.195/CNWebService/KIACGI/LeadGeneration.svc";
    //public static String SERVICE_URL = "http://192.168.56.245/hermes_net_v5/CNWebService/GULFCO/OrderManagement.svc";
    //public static String SERVICE_URL = "http://213.42.83.172/hermes_net_v5/CNWebService/GULFCO/OrderManagement.svc";
    public static String SOAP_ACTION = "http://tempuri.org/";


    public static final String SOAP_ACTION_AUTHENTICATE = "ILeadGeneration/Authenticate";
    public static final String METHOD_NAME_AUTHENTICATE = "Authenticate";
    public static final String SOAP_ACTION_GETSERVICELEVELAGREEMENT = "ILeadGeneration/GetServiceLevelAgreement";
    public static final String METHOD_NAME_GETSERVICELEVELAGREEMENT = "GetServiceLevelAgreement";
    public static final String SOAP_ACTION_GETCONTACTS = "ILeadGeneration/GetContacts";
    public static final String METHOD_NAME_GETCONTACTS = "GetContacts";
    public static final String SOAP_ACTION_GETTESTDRIVE = "ILeadGeneration/GetTestDrive";
    public static final String METHOD_NAME_GETTESTDRIVE = "GetTestDrive";
    public static final String SOAP_ACTION_GETTESTDRIVELEADS = "ILeadGeneration/GetTestDriveLeads";
    public static final String METHOD_NAME_GETTESTDRIVELEADS = "GetTestDriveLeads";


    public static String months[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    public static final String USER_INFO = "userinfo";
    public static final String REMEMBER_ME = "rememberme";
}
