package com.almajid.kialeadgen.services;

import android.content.Context;
import android.os.AsyncTask;

import com.almajid.kialeadgen.beans.ContactType;
import com.almajid.kialeadgen.beans.DailyTestDrive;
import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.beans.TestDrive;
import com.almajid.kialeadgen.beans.TestDriveLeads;
import com.almajid.kialeadgen.beans.TestDriveLeadsReasons;
import com.almajid.kialeadgen.beans.TestDriveLeadsStatus;
import com.almajid.kialeadgen.interfaces.TestDriveImpl;
import com.almajid.kialeadgen.interfaces.TestDriveLeadsImpl;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 11/21/15.
 */
public class ServiceTestDriveLeads extends AsyncTask<Void, Void, TestDriveLeads> {


    private Context context;
    private TestDriveLeadsImpl callback;
    private LeadSharedPreferences prefs;

    public ServiceTestDriveLeads(Context context, TestDriveLeadsImpl callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected TestDriveLeads doInBackground(Void... params) {

        prefs = new LeadSharedPreferences(context);
        Filter filter = prefs.getFilter();
        JSONObject json = new JSONObject();
        try {
            json.put("CodeId", 2);
            json.put("All", filter.isAll());
            json.put("Voice", filter.isVoice());
            json.put("Chat", filter.isLiveChat());
            json.put("Email", filter.isEmail());
            json.put("SocialMedia", filter.isSocialMedia());
            json.put("Walkin", filter.isWalkin());
            json.put("Individual", filter.isIndividual());
            json.put("Luxury", filter.isLuxury());
            json.put("DateRange", filter.getDateRange());
            json.put("Monthly", filter.getMonthly());
            json.put("MTD", filter.isMtd());
        } catch (JSONException e) {
            return null;
        }


        TestDriveLeads leads = new TestDriveLeads();

//        SoapObject request = new SoapObject(Global.NAMESPACE, Global.METHOD_NAME_GETTESTDRIVELEADS);
//        request.addProperty("filters", json.toString());
//
//        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        envelope.dotNet = true;
//        envelope.setOutputSoapObject(request);
//
//        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
//        try {
//            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Global.SOAP_ACTION_GETTESTDRIVELEADS), envelope);
//
//            SoapObject result = (SoapObject) envelope.getResponse();
//            if (result != null && result.getPropertyCount() > 0) {
//                SoapObject sp = (SoapObject) result.getProperty("TestDriveLeadsStatus");
//                if (sp != null) {
//                    TestDriveLeadsStatus status = new TestDriveLeadsStatus();
//                    status.setTotal(Integer.parseInt(sp.getProperty("Total").toString()));
//                    status.setOpen(Integer.parseInt(sp.getProperty("Open").toString()));
//                    status.setOpenccso(Integer.parseInt(sp.getProperty("OpenCCSO").toString()));
//                    status.setOpenccwso(Integer.parseInt(sp.getProperty("OpenCCWSO").toString()));
//                    status.setOpenwiso(Integer.parseInt(sp.getProperty("OpenWISO").toString()));
//                    status.setOpenwiwso(Integer.parseInt(sp.getProperty("OpenWIWSO").toString()));
//                    status.setWon(Integer.parseInt(sp.getProperty("Won").toString()));
//                    status.setWoncc(Integer.parseInt(sp.getProperty("WonCC").toString()));
//                    status.setWonwi(Integer.parseInt(sp.getProperty("WonWI").toString()));
//                    status.setLoss(Integer.parseInt(sp.getProperty("Loss").toString()));
//                    status.setLosscc(Integer.parseInt(sp.getProperty("LossCC").toString()));
//                    status.setLosswi(Integer.parseInt(sp.getProperty("LossWI").toString()));
//                    status.setDeadlead(Integer.parseInt(sp.getProperty("Deadlead").toString()));
//                    status.setDeadleadattcc(Integer.parseInt(sp.getProperty("DeadleadAttCC").toString()));
//                    status.setDeadleadattwi(Integer.parseInt(sp.getProperty("DeadleadAttWI").toString()));
//                    status.setDeadleadnotatt(Integer.parseInt(sp.getProperty("DeadleadNotAtt").toString()));
//                    status.setOpenpct(Integer.parseInt(sp.getProperty("OpenPct").toString()));
//                    status.setWonpct(Integer.parseInt(sp.getProperty("WonPct").toString()));
//                    status.setLosspct(Integer.parseInt(sp.getProperty("LossPct").toString()));
//                    status.setDeadleadpct(Integer.parseInt(sp.getProperty("DeadleadPct").toString()));
//
//                    leads.setTestDriveLeadsStatus(status);
//                }
//
//
//                SoapObject rls = (SoapObject) result.getProperty("ReasonsList");
//                if (rls != null) {
//
//                    List<TestDriveLeadsReasons> list = new ArrayList<>();
//
//                    for (int i=0; i<rls.getPropertyCount(); i++) {
//                        SoapObject item = (SoapObject) rls.getProperty(i);
//                        if (item != null) {
//
//                            TestDriveLeadsReasons reason = new TestDriveLeadsReasons();
//                            reason.setCode(item.getProperty("Code").toString());
//                            reason.setName(item.getProperty("Name").toString());
//                            reason.setTotal(Integer.parseInt(item.getProperty("Total").toString()));
//                            list.add(reason);
//
//                        }
//                    }
//
//                    leads.setReasonsList(list);
//                } else {
//                    List<TestDriveLeadsReasons> list = new ArrayList<>();
//                    list.add(new TestDriveLeadsReasons());
//                    leads.setReasonsList(list);
//                }
//            }
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            List<TestDriveLeadsReasons> list = new ArrayList<>();
//            list.add(new TestDriveLeadsReasons());
//            leads.setReasonsList(list);
//        }


        TestDriveLeadsStatus status = new TestDriveLeadsStatus();
        status.setTotal(138);
        status.setOpen(32);
        status.setOpenccso(1);
        status.setOpenccwso(13);
        status.setOpenwiso(0);
        status.setOpenwiwso(18);
        status.setWon(29);
        status.setWoncc(8);
        status.setWonwi(21);
        status.setLoss(21);
        status.setLosscc(21);
        status.setLosswi(0);
        status.setDeadlead(56);
        status.setDeadleadattcc(13);
        status.setDeadleadattwi(26);
        status.setDeadleadnotatt(17);
        status.setOpenpct(23);
        status.setWonpct(21);
        status.setLosspct(15);
        status.setDeadleadpct(41);
        leads.setTestDriveLeadsStatus(status);


        String code[] = {"OT","anyType{}","anyType{}","anyType{}","anyType{}","anyType{}","BNKD","anyType{}","anyType{}"};
        String name[] = {"Others","No response","No plans to purchase KIA","Not ready to buy at the moment","Purchased other brand","Downpayment exceeds the budget","Bank decline","Required car not available on time","Price"};
        int total[] = {4,0,0,0,0,0,1,0,0};

        List<TestDriveLeadsReasons> list = new ArrayList<>();

        for (int i=0; i<code.length; i++) {
            TestDriveLeadsReasons reason = new TestDriveLeadsReasons();
            reason.setCode(code[i]);
            reason.setName(name[i]);
            reason.setTotal(total[i]);
            list.add(reason);

        }

        leads.setReasonsList(list);

        return leads;
    }

    @Override
    protected void onPostExecute(TestDriveLeads leads) {
        callback.getResult(leads);
    }
}
