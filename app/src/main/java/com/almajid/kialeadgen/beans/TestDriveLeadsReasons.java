package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 11/24/15.
 */
public class TestDriveLeadsReasons {

    private int total;
    private String name;
    private String code;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
