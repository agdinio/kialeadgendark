package com.almajid.kialeadgen;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.almajid.kialeadgen.beans.TestDriveLeads;
import com.almajid.kialeadgen.interfaces.TestDriveLeadsImpl;
import com.almajid.kialeadgen.services.ServiceTestDriveLeads;
import com.androidpagecontrol.PageControl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentLeads extends Fragment {


    private View rootView;
    private ViewPager mPager;
    private PagerAdapter mPageAdapter;
    private List<Class> fragments;

    private FragmentLeadsp1 p1;
    private FragmentLeadsp2 p2;
    private FragmentLeadsp3 p3;
    private ProgressBar progress;


    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_leads));

        rootView = inflater.inflate(R.layout.fragment_leads, container, false);
        progress = (ProgressBar) rootView.findViewById(R.id.progress);
        progress.setVisibility(View.VISIBLE);

        new ServiceTestDriveLeads(rootView.getContext(),
                new TestDriveLeadsImpl() {
            @Override
            public void getResult(TestDriveLeads leads) {
                initFragments(leads);
                progress.setVisibility(View.INVISIBLE);
            }
        }).execute();

        return rootView;
    }


    @SuppressLint("NewApi")
    private void initFragments(TestDriveLeads leads) {
        fragments = new ArrayList<>();
        fragments.add(FragmentLeadsp1.class);
        fragments.add(FragmentLeadsp2.class);
        fragments.add(FragmentLeadsp3.class);

        mPageAdapter = new PageAdapter(getActivity().getSupportFragmentManager(), leads);
        mPager = (ViewPager) rootView.findViewById(R.id.pageContainer);
        mPager.setAdapter(mPageAdapter);
        mPager.setOffscreenPageLimit(0);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        if (p1 != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    p1.updateChart();
                                }
                            }, 500);
                        }
                        break;
                    case 1:
                        if (p2 != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    p2.refresh();
                                }
                            }, 500);
                        }
                        break;
                    case 2:
                        if (p3 != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    p3.updateChart();
                                }
                            }, 500);
                        }
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        PageControl pageControl = (PageControl) rootView.findViewById(R.id.page_control);
        pageControl.setViewPager(mPager);
        pageControl.setPosition(0);

        getActivity().invalidateOptionsMenu();

    }


    private class PageAdapter extends FragmentStatePagerAdapter {

        TestDriveLeads leads;

        public PageAdapter(FragmentManager fm, TestDriveLeads leads) {
            super(fm);
            this.leads = leads;
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    p1 = new FragmentLeadsp1();
                    p1.setLeads(leads);
                    return p1;
                case 1:
                    p2 = new FragmentLeadsp2();
                    p2.setLeads(leads);
                    return p2;
                case 2:
                    p3 = new FragmentLeadsp3();
                    p3.setLeads(leads);
                    return p3;
                default:
                    p1 = new FragmentLeadsp1();
                    return p1;
            }

        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

    }

}
