package com.almajid.kialeadgen.drawer;

/**
 * Created by relly on 7/1/15.
 */
public class NavMenuItem implements NavDrawerItemImpl {

    public static final int ITEM_TYPE = 1;
    private int id;
    private String label;
    private int icon;
    private String name;
    private String title;
    private boolean updateActionBarTitle;

    public static NavMenuItem create(int id, String label, int icon, String name, String title) {
        NavMenuItem item = new NavMenuItem();
        item.setLabel(label);
        item.setIcon(icon);
        item.setName(name);
        item.setTitle(title);

        return item;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getType() {
        return ITEM_TYPE;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean updateActionBarTitle() {
        return this.updateActionBarTitle;
    }

    public void setUpdateActionBarTitle(boolean updateActionBarTitle) {
        this.updateActionBarTitle = updateActionBarTitle;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
