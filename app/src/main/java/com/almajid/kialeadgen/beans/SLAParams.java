package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 11/14/15.
 */
public class SLAParams {

    private int codeId;
    private String statusCode;
    private boolean all;
    private boolean voice;
    private boolean chat;
    private boolean email;
    private boolean socialMedia;
    private boolean walkin;
    private boolean individual;
    private boolean luxury;
    private String dateRange;
    private int monthly;
    private boolean ytd;

    public int getCodeId() {
        return codeId;
    }

    public void setCodeId(int codeId) {
        this.codeId = codeId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isAll() {
        return all;
    }

    public void setAll(boolean all) {
        this.all = all;
    }

    public boolean isVoice() {
        return voice;
    }

    public void setVoice(boolean voice) {
        this.voice = voice;
    }

    public boolean isChat() {
        return chat;
    }

    public void setChat(boolean chat) {
        this.chat = chat;
    }

    public boolean isEmail() {
        return email;
    }

    public void setEmail(boolean email) {
        this.email = email;
    }

    public boolean isSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(boolean socialMedia) {
        this.socialMedia = socialMedia;
    }

    public boolean isWalkin() {
        return walkin;
    }

    public void setWalkin(boolean walkin) {
        this.walkin = walkin;
    }

    public boolean isIndividual() {
        return individual;
    }

    public void setIndividual(boolean individual) {
        this.individual = individual;
    }

    public boolean isLuxury() {
        return luxury;
    }

    public void setLuxury(boolean luxury) {
        this.luxury = luxury;
    }

    public String getDateRange() {
        return dateRange;
    }

    public void setDateRange(String dateRange) {
        this.dateRange = dateRange;
    }

    public int getMonthly() {
        return monthly;
    }

    public void setMonthly(int monthly) {
        this.monthly = monthly;
    }

    public boolean isYtd() {
        return ytd;
    }

    public void setYtd(boolean ytd) {
        this.ytd = ytd;
    }
}
