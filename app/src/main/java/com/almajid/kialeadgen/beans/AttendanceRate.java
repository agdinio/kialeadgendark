package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 11/15/15.
 */
public class AttendanceRate {

    private int bookedCC;
    private int attendedCC;
    private int attendedCCPct;
    private int attendedTodayCCPct;
    private int attendedTarget;
    private String attendedGraph;

    public int getBookedCC() {
        return bookedCC;
    }

    public void setBookedCC(int bookedCC) {
        this.bookedCC = bookedCC;
    }

    public int getAttendedCC() {
        return attendedCC;
    }

    public void setAttendedCC(int attendedCC) {
        this.attendedCC = attendedCC;
    }

    public int getAttendedCCPct() {
        return attendedCCPct;
    }

    public void setAttendedCCPct(int attendedCCPct) {
        this.attendedCCPct = attendedCCPct;
    }

    public int getAttendedTodayCCPct() {
        return attendedTodayCCPct;
    }

    public void setAttendedTodayCCPct(int attendedTodayCCPct) {
        this.attendedTodayCCPct = attendedTodayCCPct;
    }

    public int getAttendedTarget() {
        return attendedTarget;
    }

    public void setAttendedTarget(int attendedTarget) {
        this.attendedTarget = attendedTarget;
    }

    public String getAttendedGraph() {
        return attendedGraph;
    }

    public void setAttendedGraph(String attendedGraph) {
        this.attendedGraph = attendedGraph;
    }
}
