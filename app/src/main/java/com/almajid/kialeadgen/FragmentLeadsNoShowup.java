package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentLeadsNoShowup extends Fragment {


    private View rootView;
    private HorizontalBarChart leadsChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_leads_no_showup, container, false);
        leadsChart = (HorizontalBarChart) rootView.findViewById(R.id.leadsChart);

        return rootView;
    }

    public void updateChart() {

        leadsChart.setDrawBarShadow(false);
        leadsChart.setDrawValueAboveBar(true);
        leadsChart.setDescription("");
        leadsChart.setPinchZoom(false);
        leadsChart.setDrawGridBackground(false);
        leadsChart.getLegend().setEnabled(false);
        leadsChart.getAxisLeft().setDrawLabels(false);
        leadsChart.animateY(2500);

        XAxis xl = leadsChart.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setDrawAxisLine(true);
        xl.setDrawGridLines(true);
        xl.setGridLineWidth(0.3f);
        xl.setTextColor(Color.WHITE);
        xl.setTextSize(8f);

        YAxis yr = leadsChart.getAxisRight();
        yr.setDrawAxisLine(true);
        yr.setDrawGridLines(false);
        yr.setTextColor(Color.WHITE);
        yr.setTextSize(8f);

        Legend l = leadsChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setFormSize(8f);
        l.setXEntrySpace(4f);


        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();


        xVals.add("Prefer Different Brand");
        xVals.add("Declined");
        xVals.add("Not Interested Anymore");
        xVals.add("Unavailability of the Car");

        yVals1.add(new BarEntry(15, 0));
        yVals1.add(new BarEntry(45, 1));
        yVals1.add(new BarEntry(98, 2));
        yVals1.add(new BarEntry(34, 3));

        BarDataSet set1 = new BarDataSet(yVals1, "No Showup Reasons");
        set1.setColor(Color.YELLOW);
        set1.setValueTextColor(Color.WHITE);

        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(set1);

        BarData data = new BarData(xVals, dataSets);
        data.setValueTextSize(8f);

        leadsChart.setData(data);

    }

}
