package com.almajid.kialeadgen.persistents;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by relly on 11/24/15.
 */
public class SqlHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "leadgen.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TBL_CLOSINGRATE = "tbl_closingrate";
    public static final String COL_CLOSINGRATE_WON = "closedwon";
    public static final String COL_CLOSINGRATE_QUALIFIED = "qualified";
    public static final String COL_CLOSINGRATE_TARGET = "target";
    public static final String COL_CLOSINGRATE_WONPCT = "wonpct";
    public static final String COL_CLOSINGRATE_WONCC = "woncc";
    public static final String COL_CLOSINGRATE_WONWI = "winwi";
    public static final String COL_CLOSINGRATE_WONCCPCT = "wonccpct";
    public static final String COL_CLOSINGRATE_WONWIPCT = "wonwipct";
    public static final String COL_CLOSINGRATE_WONGRAPH = "wongraph";
    public static final String CREATE_TBL_CLOSINGRATE = "create table "
                                + TBL_CLOSINGRATE + "("
                                + COL_CLOSINGRATE_WON + " integer, "
                                + COL_CLOSINGRATE_QUALIFIED + " integer, "
                                + COL_CLOSINGRATE_TARGET + " integer, "
                                + COL_CLOSINGRATE_WONPCT + " integer, "
                                + COL_CLOSINGRATE_WONCC + " integer, "
                                + COL_CLOSINGRATE_WONWI + " integer, "
                                + COL_CLOSINGRATE_WONCCPCT + " integer, "
                                + COL_CLOSINGRATE_WONWIPCT + " integer, "
                                + COL_CLOSINGRATE_WONGRAPH + " text "
                                + ")";

    public static final String TBL_VOC = "tbl_voc";
    public static final String COL_VOC_ACTUALSCORE = "actualscore";
    public static final String COL_VOC_MAXSCORE = "maxscore";
    public static final String COL_VOC_TARGET = "target";
    public static final String COL_VOC_AVERAGE = "average";
    public static final String COL_VOC_GRAPH = "graph";
    public static final String CREATE_TBL_VOC = "create table "
                                + TBL_VOC + "("
                                + COL_VOC_ACTUALSCORE + " integer, "
                                + COL_VOC_MAXSCORE + " integer, "
                                + COL_VOC_TARGET + " integer, "
                                + COL_VOC_AVERAGE + " integer, "
                                + COL_VOC_GRAPH + " text "
                                + ")";



    public SqlHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TBL_CLOSINGRATE);
        db.execSQL(CREATE_TBL_VOC);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SqlHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");

        db.execSQL("DROP TABLE IF EXISTS " + TBL_CLOSINGRATE);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_VOC);
        onCreate(db);

    }
}
