package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.ContactType;
import com.almajid.kialeadgen.beans.TestDrive;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;

/**
 * Created by relly on 9/7/15.
 */
public class FragmentTestDrivep2 extends Fragment {


    private View rootView;
    private PieChart ctChart;
    private TestDrive testDrive;

    private TextView txtIndividual;
    private TextView txtLuxury;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (testDrive != null) {
            rootView = inflater.inflate(R.layout.fragment_testdrive_p2, container, false);
            ctChart = (PieChart) rootView.findViewById(R.id.ctChart);
            txtIndividual = (TextView) rootView.findViewById(R.id.txtIndividual);
            txtLuxury = (TextView) rootView.findViewById(R.id.txtLuxury);

            updateLabels();
            updateChart();
        } else {
            Toast.makeText(getActivity().getBaseContext(), "Server Connection Error: Data cannot be retrieved. Please try again.", Toast.LENGTH_LONG).show();
        }


        return rootView;
    }

    public void setTestDrive(TestDrive testDrive) {
        this.testDrive = testDrive;
    }

    public void updateLabels() {

        ContactType ct = testDrive.getContactType();

        txtIndividual.setText(String.valueOf(ct.getIndividual()));
        txtLuxury.setText(String.valueOf(ct.getLuxury()));
    }

    public void updateChart() {

        ContactType ct = testDrive.getContactType();

        ctChart.setUsePercentValues(true);
        ctChart.setDescription("");
        ctChart.setDragDecelerationFrictionCoef(0.95f);

        ctChart.setDrawHoleEnabled(true);
        ctChart.setHoleColorTransparent(true);

        ctChart.setTransparentCircleColor(Color.WHITE);
        ctChart.setTransparentCircleAlpha(50);

        ctChart.setHoleRadius(50f);
        ctChart.setTransparentCircleRadius(55f);

        ctChart.setDrawCenterText(true);
        ctChart.setDrawSliceText(false);

        ctChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        ctChart.setRotationEnabled(false);
        ctChart.animateY(1500, Easing.EasingOption.EaseInOutQuad);

        Legend l = ctChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_CENTER);
        l.setTextColor(Color.WHITE);
        l.setTextSize(8f);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        float mult = 100;
        int count = 2;

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        yVals1.add(new Entry(ct.getIndividualPct(), 0));
        yVals1.add(new Entry(ct.getLuxuryPct(), 1));


        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("INDIVIDUAL");
        xVals.add("LUXURY");

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(5f);



        ArrayList<Integer> colors = new ArrayList<Integer>();
        colors.add(Color.parseColor("#162d46"));
        colors.add(Color.parseColor("#461C1F"));


        dataSet.setColors(colors);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(8f);
        data.setValueTextColor(Color.WHITE);

        ctChart.setData(data);

        // undo all highlights
        ctChart.highlightValues(null);

        ctChart.invalidate();

    }

}
