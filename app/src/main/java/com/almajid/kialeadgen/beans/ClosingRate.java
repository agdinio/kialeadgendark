package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 10/6/15.
 */
public class ClosingRate {

    private int closedWon;
    private int qualified;
    private int target;
    private int closedWonPct;
    private int closedWonCC;
    private int closedWonWI;
    private int closedWonCCPct;
    private int closedWonWIPct;
    private String closedWonGraph;

    public int getClosedWon() {
        return closedWon;
    }

    public void setClosedWon(int closedWon) {
        this.closedWon = closedWon;
    }

    public int getQualified() {
        return qualified;
    }

    public void setQualified(int qualified) {
        this.qualified = qualified;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public int getClosedWonPct() {
        return closedWonPct;
    }

    public void setClosedWonPct(int closedWonPct) {
        this.closedWonPct = closedWonPct;
    }

    public int getClosedWonCC() {
        return closedWonCC;
    }

    public void setClosedWonCC(int closedWonCC) {
        this.closedWonCC = closedWonCC;
    }

    public int getClosedWonWI() {
        return closedWonWI;
    }

    public void setClosedWonWI(int closedWonWI) {
        this.closedWonWI = closedWonWI;
    }

    public int getClosedWonCCPct() {
        return closedWonCCPct;
    }

    public void setClosedWonCCPct(int closedWonCCPct) {
        this.closedWonCCPct = closedWonCCPct;
    }

    public int getClosedWonWIPct() {
        return closedWonWIPct;
    }

    public void setClosedWonWIPct(int closedWonWIPct) {
        this.closedWonWIPct = closedWonWIPct;
    }

    public String getClosedWonGraph() {
        return closedWonGraph;
    }

    public void setClosedWonGraph(String closedWonGraph) {
        this.closedWonGraph = closedWonGraph;
    }
}
