package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

/**
 * Created by relly on 9/8/15.
 */
public class FragmentFilter extends Fragment {

    private View rootView;
    private FragmentTabHost tabhost;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_filter));

        rootView = inflater.inflate(R.layout.fragment_filter, container, false);

        tabhost = (FragmentTabHost) rootView.findViewById(R.id.tabhost);
        tabhost.setup(rootView.getContext(), getChildFragmentManager(), R.id.realtabcontent);
        tabhost.addTab(tabhost.newTabSpec("0").setIndicator("DATE"), FragmentFilterDate.class, null);
        tabhost.addTab(tabhost.newTabSpec("1").setIndicator("ADVANCE"), FragmentFilterAdvance.class, null);
        tabhost.getTabWidget().setStripEnabled(false);
        tabhost.getTabWidget().setDividerDrawable(null);

        tabSetResource();

        tabhost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                tabSetResource();
            }
        });

        return rootView;
    }

    private void tabSetResource() {
        for (int i=0; i<tabhost.getTabWidget().getChildCount(); i++) {
            View v = tabhost.getTabWidget().getChildAt(i);

            v.setBackgroundResource(R.drawable.date_filter_tab_default);
            TextView tv = (TextView) tabhost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(Color.argb(255, 255, 255, 255));
        }
        TextView tv = (TextView) tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab()).findViewById(android.R.id.title);
        tv.setTextColor(getResources().getColor(R.color.primary));
    }

}
