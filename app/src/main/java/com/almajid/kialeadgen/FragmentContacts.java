package com.almajid.kialeadgen;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.almajid.kialeadgen.beans.Contacts;
import com.almajid.kialeadgen.beans.ServiceLevelAgreement;
import com.almajid.kialeadgen.interfaces.ContactsImpl;
import com.almajid.kialeadgen.services.ServiceContacts;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by relly on 11/21/15.
 */
public class FragmentContacts extends Fragment {

    private View rootView;
    private ProgressBar progress;
    private PagerAdapter mPageAdapter;
    private ViewPager mPager;
    private List<Class> fragments;

    private FragmentContactsp1 p1;

    @SuppressLint("NewApi")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_contacts));

        rootView = inflater.inflate(R.layout.fragment_sla, container, false);
        progress = (ProgressBar) rootView.findViewById(R.id.progress);
        progress.setVisibility(View.VISIBLE);

        new ServiceContacts(rootView.getContext(),
                new ContactsImpl() {
                    @Override
                    public void getResult(Contacts contacts) {
                        initFragments(contacts);
                        progress.setVisibility(View.INVISIBLE);
                    }
                }).execute();

        getActivity().invalidateOptionsMenu();

        return rootView;
    }


    private void initFragments(Contacts contacts) {
        fragments = new ArrayList<>();
        fragments.add(FragmentContactsp1.class);

        mPageAdapter = new PageAdapter(getActivity().getSupportFragmentManager(), contacts);
        mPager = (ViewPager) rootView.findViewById(R.id.pageContainer);
        mPager.setAdapter(mPageAdapter);
        mPager.setOffscreenPageLimit(0);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        if (p1 != null) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    p1.updateLabels();
                                    p1.updateChart();
                                }
                            }, 500);
                        }
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }


    private class PageAdapter extends FragmentStatePagerAdapter {

        private Contacts contacts;

        public PageAdapter(FragmentManager fm, Contacts contacts) {
            super(fm);
            this.contacts = contacts;
        }

        @Override
        public Fragment getItem(int position) {
                switch(position) {
                case 0:
                    p1 = new FragmentContactsp1();
                    p1.setContacts(contacts);
                    return p1;
                default:
                    p1 = new FragmentContactsp1();
                    return p1;
            }

        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

//        @Override
//        public boolean isViewFromObject(View view, Object object) {
//            if (object != null) {
//                return ((Fragment)object).getView() == view;
//            } else {
//                return false;
//            }
//        }

        @Override
        public int getCount() {
            return fragments.size();
        }

    }

}
