package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 10/6/15.
 */
public class ServiceLevelAgreement {

    private ClosingRate closingRate;
    private VoiceOfCustomer voc;
    private AttendanceRate attendanceRate;
    private QualificationRate qualificationRate;
    private Quality quality;

    public ClosingRate getClosingRate() {
        return closingRate;
    }

    public void setClosingRate(ClosingRate closingRate) {
        this.closingRate = closingRate;
    }

    public VoiceOfCustomer getVoc() {
        return voc;
    }

    public void setVoc(VoiceOfCustomer voc) {
        this.voc = voc;
    }

    public AttendanceRate getAttendanceRate() {
        return attendanceRate;
    }

    public void setAttendanceRate(AttendanceRate attendanceRate) {
        this.attendanceRate = attendanceRate;
    }

    public QualificationRate getQualificationRate() {
        return qualificationRate;
    }

    public void setQualificationRate(QualificationRate qualificationRate) {
        this.qualificationRate = qualificationRate;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }
}
