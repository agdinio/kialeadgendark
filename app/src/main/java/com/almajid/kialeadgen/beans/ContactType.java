package com.almajid.kialeadgen.beans;

/**
 * Created by relly on 11/22/15.
 */
public class ContactType {

    private int total;
    private int individual;
    private int luxury;
    private int individualPct;
    private int luxuryPct;

    public int getIndividual() {
        return individual;
    }

    public void setIndividual(int individual) {
        this.individual = individual;
    }

    public int getLuxury() {
        return luxury;
    }

    public void setLuxury(int luxury) {
        this.luxury = luxury;
    }

    public int getIndividualPct() {
        return individualPct;
    }

    public void setIndividualPct(int individualPct) {
        this.individualPct = individualPct;
    }

    public int getLuxuryPct() {
        return luxuryPct;
    }

    public void setLuxuryPct(int luxuryPct) {
        this.luxuryPct = luxuryPct;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
