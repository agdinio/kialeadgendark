package com.almajid.kialeadgen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.almajid.kialeadgen.beans.Contacts;
import com.almajid.kialeadgen.beans.DailyContacts;
import com.almajid.kialeadgen.beans.DailyTestDrive;
import com.almajid.kialeadgen.beans.Filter;
import com.almajid.kialeadgen.beans.TestDrive;
import com.almajid.kialeadgen.persistents.LeadSharedPreferences;
import com.almajid.kialeadgen.statics.Global;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by relly on 9/8/15.
 */
public class FragmentTestDrivep1 extends Fragment {

    private LeadSharedPreferences prefs;
    private View rootView;
    private LineChart dtChart;
    private TestDrive testDrive;

    private TextView txtTotalBookings;
    private TextView txtAttended;
    private TextView txtAttendedCC;
    private TextView txtAttendedWI;
    private TextView txtNotAttended;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (testDrive != null) {
            rootView = inflater.inflate(R.layout.fragment_testdrive_p1, container, false);
            dtChart = (LineChart) rootView.findViewById(R.id.dtChart);
            txtTotalBookings = (TextView) rootView.findViewById(R.id.txtTotalBookings);
            txtAttended = (TextView) rootView.findViewById(R.id.txtAttended);
            txtAttendedCC = (TextView) rootView.findViewById(R.id.txtAttendedCC);
            txtAttendedWI = (TextView) rootView.findViewById(R.id.txtAttendedWI);
            txtNotAttended = (TextView) rootView.findViewById(R.id.txtNotAttended);

            updateLabels();
            updateChart();
        } else {
            Toast.makeText(getActivity().getBaseContext(), "Server Connection Error: Unable to connect.", Toast.LENGTH_LONG).show();
        }

        return rootView;
    }

    public void setTestDrive(TestDrive testDrive) {
        this.testDrive = testDrive;
    }

    public void updateLabels() {
        DailyTestDrive dt = testDrive.getDailyTestDrive();

        if (dt == null) {
            Toast.makeText(getActivity().getBaseContext(), "Server Connection Error: Unable to connect.", Toast.LENGTH_LONG).show();
            return;
        }

        if (dt != null) {
            txtTotalBookings.setText(String.valueOf(dt.getTotalBookings()));
            txtAttended.setText(String.valueOf(dt.getAttended()));
            txtAttendedCC.setText(String.valueOf(dt.getAttendedCC()));
            txtAttendedWI.setText(String.valueOf(dt.getAttendedWI()));
            txtNotAttended.setText(String.valueOf(dt.getNotAttended()));
        }
    }

    public void updateChart() {
        prefs = new LeadSharedPreferences(rootView.getContext());
        Filter filter = prefs.getFilter();
        if (filter == null) {
            return;
        }

        DailyTestDrive dt = testDrive.getDailyTestDrive();
        if (dt == null) {
            Toast.makeText(getActivity().getBaseContext(), "Server Connection Error: Unable to connect.", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(dt.getGraph()))
        {
            return;
        }

        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> values = new ArrayList<Entry>();


        String dca[] = TextUtils.isEmpty(dt.getGraph()) ? new String[]{} : dt.getGraph().split(",");

        if (filter.getFilterType().equalsIgnoreCase("MONTHLY")) {
            SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
            Calendar calFilterDate = Calendar.getInstance();
            Calendar calDateWithValue = Calendar.getInstance();
            Date filterdate = null;

            String curr_filters[] = filter.getMonthly().split(",");

            for (int i = 0; i < curr_filters.length; i++) {
                try {
                    filterdate = df.parse(curr_filters[i]);
                    calFilterDate.setTime(filterdate);

                    String str_month = Global.months[calFilterDate.get(Calendar.MONTH)];

                    xVals.add(str_month + " " + calFilterDate.get(Calendar.YEAR));
                    values.add(new Entry(0, i));

                    for (int j = 0; j < dca.length; j++) {


                        String ar[] = dca[j].split("=");

                        Date datewithvalue = df.parse(ar[0]);
                        calDateWithValue.setTime(datewithvalue);

                        if (calDateWithValue.get(Calendar.MONTH) == calFilterDate.get(Calendar.MONTH)
                                && calDateWithValue.get(Calendar.YEAR) == calFilterDate.get(Calendar.YEAR)) {
                            int e = Integer.parseInt(ar[1]);
                            values.set(i, new Entry(e, i));
                        }

                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

        } else if (filter.getFilterType().equalsIgnoreCase("DATERANGE")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM-dd-''yy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            String curr_filters[] = filter.getDateRange().split(",");

            try {
                Date sdate = dffrom.parse(curr_filters[0]);
                Date edate = dfto.parse(curr_filters[1]);
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new Entry(0, i));

                for (int j=0; j<dca.length; j++) {

                    String ar[] = dca[j].split("=");

                    if (ar[0].equalsIgnoreCase((dfto.format(targetday)))) {
                        //float e = Math.round((Float.parseFloat(ar[1]) / Float.parseFloat(ar[2])) * 100);
                        int e = Integer.parseInt(ar[1]);
                        values.set(i, new Entry(e, i));
                    }
                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        } else if (filter.getFilterType().equalsIgnoreCase("MTD")) {
            SimpleDateFormat dffrom = new SimpleDateFormat("MM-01-yyyy");
            SimpleDateFormat dfto = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dfVal = new SimpleDateFormat("MM-dd-''yy");

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            try {
                Date sdate = dffrom.parse(dffrom.format(new Date()));
                Date edate = dfto.parse(dfto.format(new Date()));
                start.setTime(sdate);
                end.setTime(edate);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            int i = 0;
            while (!start.after(end)) {
                Date targetday = start.getTime();

                xVals.add(dfVal.format(targetday));
                values.add(new Entry(0, i));

                for (int j=0; j<dca.length; j++) {


                    String ar[] = dca[j].split("=");

                    if (ar[0].equalsIgnoreCase(dfto.format(targetday))) {
                        int e = Integer.parseInt(ar[1]);
                        values.set(i, new Entry(e, i));
                    }

                }

                start.add(Calendar.DATE, 1);
                i++;
            }

        }


        dtChart.setDrawGridBackground(false);
        dtChart.setDescription("");
        //crChart.setHighlightEnabled(false);
        //crChart.setTouchEnabled(false);
        //crChart.setScaleEnabled(true);
        dtChart.setPinchZoom(true);

        dtChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
        dtChart.resetTracking();
        dtChart.getAxisRight().setDrawLabels(false);
        dtChart.getAxisLeft().setDrawLabels(false);
        dtChart.getLegend().setEnabled(false);




        XAxis xaxis = dtChart.getXAxis();
        xaxis.setTextSize(8f);
        xaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        xaxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis lyaxis = dtChart.getAxisLeft();
        lyaxis.setTextSize(8f);
        lyaxis.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        //lyaxis.setAxisMaxValue(15f);

        LineDataSet d = new LineDataSet(values, "Daily Testdrive");
        d.setValueTextSize(7f);
        d.setColor(Color.YELLOW);
        d.setDrawCircles(false);
        d.setValueTextColor(ContextCompat.getColor(rootView.getContext(), R.color.base_bg));
        //d.setValueFormatter(new PercentFormatter());
        LineData data = new LineData(xVals, d);

        dtChart.setData(data);
        dtChart.invalidate();

    }
}
