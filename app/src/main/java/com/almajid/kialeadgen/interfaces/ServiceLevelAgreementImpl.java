package com.almajid.kialeadgen.interfaces;

import com.almajid.kialeadgen.beans.ServiceLevelAgreement;

/**
 * Created by relly on 10/6/15.
 */
public interface ServiceLevelAgreementImpl {

    void getResult(ServiceLevelAgreement sla);

}
